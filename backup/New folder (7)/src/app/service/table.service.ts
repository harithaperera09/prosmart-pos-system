import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Table } from '../dto/table';
import { MAIN_URL } from '../service/dataservice.service';



@Injectable()
export class TableService {

  constructor(private http: HttpClient) { }

  // addTable(customer: Table): Observable<boolean> {
  //   return this.http.post<boolean>(MAIN_URL + URL, customer);
  // }

  // editeTable(customer: Table): Observable<boolean> {
  //   return this.http.post<boolean>(MAIN_URL + URL, customer);
  // }

  // deleteTable(id: string): Observable<boolean> {
  //   return this.http.delete<boolean>(MAIN_URL + URL + '/' + id);
  // }

  // deactivateTable(id: string): Observable<boolean> {
  //   return this.http.delete<boolean>(MAIN_URL + URL + '/' + id);
  // }

  // getTable(id: String): Observable<Table> {
  //   return this.http.get<Table>(MAIN_URL + URL + '/' + id);
  // }

  // getAllTables(): Observable<Array<Table>> {
  //   return this.http.get<Array<Table>>(MAIN_URL + URL);
  // }

  getAllActiveTableByLocationStatus(saLocationId: number): Observable<Array<Table>> {
    return this.http.get<Array<Table>>(MAIN_URL + '/getallactivetablebylocationstatus-' + saLocationId);
  }


  // getAllActiveTables(): Observable<Array<Table>> {
  //   return this.http.get<Array<Table>>(MAIN_URL + URL);
  // }
}
