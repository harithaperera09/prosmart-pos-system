import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingtableComponent } from './routingtable/routingtable.component';
import { RoutingtableRouterModule } from './routingtable-router.module';


@NgModule({
  declarations: [RoutingtableComponent],
  imports: [
    CommonModule,
    RoutingtableRouterModule
  ]
})
export class RoutingtableModule { }
