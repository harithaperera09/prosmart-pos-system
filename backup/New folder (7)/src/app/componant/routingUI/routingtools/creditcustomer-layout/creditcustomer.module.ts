import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { CreditcustomerComponent } from './creditcustomer/creditcustomer.component';
import { CreditcustomerRouterModule } from './creditcustomer-router.module';


@NgModule({
  declarations: [
    CreditcustomerComponent
  ],
  imports: [
    CommonModule,
    CreditcustomerRouterModule,
    Ng2CarouselamosModule
  ]
})
export class CreditcustomerModule { }
