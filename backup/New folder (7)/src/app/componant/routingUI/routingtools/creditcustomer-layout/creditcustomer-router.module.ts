import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CreditcustomerComponent } from './creditcustomer/creditcustomer.component';


const routes: Routes = [
  {
    path : '',
    component : CreditcustomerComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class CreditcustomerRouterModule { }
