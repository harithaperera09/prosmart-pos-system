import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { GuestspecialsComponent } from './guestspecials/guestspecials.component';
import { GuestspecialsRouterModule } from './guestspecials-router.module';


@NgModule({
  declarations: [
    GuestspecialsComponent
  ],
  imports: [
    CommonModule,
    GuestspecialsRouterModule,
    Ng2CarouselamosModule
  ]
})
export class GuestspecialsModule { }
