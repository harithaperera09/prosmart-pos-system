import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { GuestspecialsComponent } from './guestspecials/guestspecials.component';


const routes: Routes = [
  {
    path : '',
    component : GuestspecialsComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class GuestspecialsRouterModule { }
