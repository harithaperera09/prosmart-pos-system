import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MergebillComponent } from './mergebill/mergebill.component';


const routes: Routes = [
  {
    path : '',
    component : MergebillComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class MergebillRouterModule { }
