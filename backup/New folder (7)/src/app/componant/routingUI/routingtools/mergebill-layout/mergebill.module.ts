import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { MergebillComponent } from './mergebill/mergebill.component';
import { MergebillRouterModule } from './mergebill-router.module';


@NgModule({
  declarations: [
    MergebillComponent
  ],
  imports: [
    CommonModule,
    MergebillRouterModule,
    Ng2CarouselamosModule
  ]
})
export class MergebillModule { }
