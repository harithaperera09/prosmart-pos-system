import { Component, OnInit } from '@angular/core';
import { MergebillService } from 'src/app/service/mergebill.service';
import { BillMergeDto } from 'src/app/dto/BillMergeDto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mergebill',
  templateUrl: './mergebill.component.html',
  styleUrls: ['./mergebill.component.scss']
})
export class MergebillComponent implements OnInit {

  constructor(
    private mergebillService: MergebillService,
    private router: Router) { }

  lists1: Map<any, String> = new Map<any, String>();
  lists2: Map<any, String> = new Map<any, String>();

  tablerows: Array<BillMergeDto> = [];

  ngOnInit() {
    this.getAllBillList();
  }

  getAllBillList() {
    this.mergebillService.getAllBillList().subscribe(
      (result) => {
        result.forEach(element => {
          this.lists1.set(element, element);
          // this.lists2.set(element, element);
        });
      });
  }

  homeAction() {
    this.router.navigate(['home']);
  }

  toolAction() {
    this.router.navigate(['tools/tooldashbord']);
  }

  margefistAction1(name: String) {
    if (name !== null || name !== undefined || name !== '') {
      this.lists2 = new Map<any, String>();

      this.mergebillService.getAllBillList().subscribe(
        (resultt) => {
          resultt.forEach(elementt => {
            if (elementt !== name) {
              this.lists2.set(elementt, elementt);
            }
            return;
          });
        });
      this.lists2.delete(name);

      this.mergebillService.findbillbyID(name).subscribe(result => {
        this.tablerows = result;
      });
    }
  }

  SelectTableValue(name: String) {

  }

  resetAction() {
    this.lists1 = new Map<any, String>();
    this.lists2 = new Map<any, String>();
    this.getAllBillList();
  }



}
