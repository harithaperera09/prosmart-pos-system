import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { CreatetableComponent } from './createtable/createtable.component';
import { CreatetableRouterModule } from './createtable-router.module';


@NgModule({
  declarations: [
    CreatetableComponent
  ],
  imports: [
    CommonModule,
    CreatetableRouterModule,
    Ng2CarouselamosModule
  ]
})
export class CreatetableModule { }
