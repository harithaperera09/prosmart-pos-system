import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CreatetableComponent } from './createtable/createtable.component';


const routes: Routes = [
  {
    path : '',
    component : CreatetableComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class CreatetableRouterModule { }
