
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BilltransferComponent } from './billtransfer/billtransfer.component';


const routes: Routes = [
  {
    path : '',
    component : BilltransferComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class BilltransferRouterModule { }
