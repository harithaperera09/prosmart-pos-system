
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { BilltransferComponent } from './billtransfer/billtransfer.component';
import { BilltransferRouterModule } from './billtransfer-router.module';


@NgModule({
  declarations: [
    BilltransferComponent,
  ],
  imports: [
    CommonModule,
    BilltransferRouterModule,
    Ng2CarouselamosModule
  ]
})
export class BilltransferModule { }
