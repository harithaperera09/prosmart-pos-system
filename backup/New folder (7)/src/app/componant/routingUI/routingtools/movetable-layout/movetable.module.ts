import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { MovetableComponent } from './movetable/movetable.component';
import { MovetableRouterModule } from './movetable-router.module';


@NgModule({
  declarations: [
    MovetableComponent
  ],
  imports: [
    CommonModule,
    MovetableRouterModule,
    Ng2CarouselamosModule
  ]
})
export class MovetableModule { }
