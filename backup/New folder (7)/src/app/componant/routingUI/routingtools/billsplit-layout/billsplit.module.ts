import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
// import { BillSplitComponent } from 'backup/New folder (4)/src/app/componant/routingUI/routingtools/bill-split/bill-split.component';
import { BillSplitRouterModule } from './billsplit-router.module';
import { BillSplitComponent } from './bill-split/bill-split.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    BillSplitComponent,
  ],
  imports: [
    CommonModule,
    BillSplitRouterModule,
    Ng2CarouselamosModule,
    FormsModule
  ]
})
export class  BillSplitModule { }
