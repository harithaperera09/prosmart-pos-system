import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
// import { RoutingitmesComponent } from './routingitmes/routingitmes.component';
import { BillSplitComponent } from './bill-split/bill-split.component';


const routes: Routes = [
  {
    path : '',
    component : BillSplitComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class BillSplitRouterModule { }
