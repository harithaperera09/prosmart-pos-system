import { Component, OnInit } from '@angular/core';
import { BillSplitService } from 'src/app/service/bill-split.service';
import { Settlement } from 'src/app/dto/settlement';
import { MasterLine } from 'src/app/dto/masterline';

import { Router } from '@angular/router';
@Component({
  selector: 'app-bill-split',
  templateUrl: './bill-split.component.html',
  styleUrls: ['./bill-split.component.scss']
})
export class BillSplitComponent implements OnInit {

  ProceedTableOrders = new Map<any, Settlement>();
  Proceeditems = new Map<any, MasterLine>();
  temProceeditem = new MasterLine();
  temProceeditemmap = new Map<any, MasterLine>();

  itemcode;
  totalqty1;
  totalqty2;

  constructor(
    private billSplitservice: BillSplitService,
    private router: Router) { }

  ngOnInit() {
    this.loadAllProceedTableOrders();
  }


  homeAction() {
    this.router.navigate(['home']);
  }

  toolAction() {
    this.router.navigate(['tools/tooldashbord']);
  }

  loadAllProceedTableOrders() {
    this.billSplitservice.loadAllProceedTableOrders().subscribe(
      (result) => {
        result.forEach(element => {
          this.ProceedTableOrders.set(element.saPosMasterId, element);
        });
        return;
      }
    );

  }
  tableclickAction(saPosMasterId) {
    this.Proceeditems = new Map<any, MasterLine>();
    this.billSplitservice.loadAllProceedItems(saPosMasterId).subscribe(
      (result) => {
        result.forEach(elementt => {
          this.Proceeditems.set(elementt.itemMasterId, elementt);
        });
        return;
      }
    );
  }

  orderdetailsTableAction(masterline: MasterLine) {
    this.itemcode = '';
    this.totalqty1 = '';
    this.totalqty2 = '';
    this.itemcode = masterline.itemMasterId.toString();
    this.totalqty1 = masterline.quentity.toString();
    this.totalqty2 = masterline.quentity.toString();
  }


  addAction() {
    this.Proceeditems.forEach(addelement => {
      if (addelement.itemMasterId === this.itemcode) {
        if (this.totalqty1 > this.totalqty2) {
          this.temProceeditem = addelement;
          this.temProceeditem.quentity = this.totalqty2;
          this.temProceeditemmap.set(this.temProceeditem.itemMasterId, this.temProceeditem);

          addelement.quentity = addelement.quentity - this.totalqty2;


        } else {
          alert('invalid input');
        }


      }
    });
    return;
  }


}
