import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReprintComponent } from './reprint/reprint.component';


const routes: Routes = [
  {
    path : '',
    component : ReprintComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class ReprintRouterModule { }
