import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { ReprintComponent } from './reprint/reprint.component';
import { ReprintRouterModule } from './reprint-router.module';


@NgModule({
  declarations: [
    ReprintComponent
  ],
  imports: [
    CommonModule,
    ReprintRouterModule,
    Ng2CarouselamosModule
  ]
})
export class ReprintModule { }
