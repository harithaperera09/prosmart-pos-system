import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { CashinComponent } from './cashin/cashin.component';
import { CashinRouterModule } from './cashin-router.module';


@NgModule({
  declarations: [
    CashinComponent
  ],
  imports: [
    CommonModule,
    CashinRouterModule,
    Ng2CarouselamosModule
  ]
})
export class CashinModule { }
