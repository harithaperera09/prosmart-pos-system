import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RoominglistComponent } from './roominglist/roominglist.component';


const routes: Routes = [
  {
    path : '',
    component : RoominglistComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class RoominglistRouterModule { }
