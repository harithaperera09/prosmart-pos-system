import { Component, OnInit } from '@angular/core';
import { ReservationLine } from 'src/app/dto/reservationline';
import { InhouseService } from 'src/app/service/inhouse.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-roominglist',
  templateUrl: './roominglist.component.html',
  styleUrls: ['./roominglist.component.scss']
})
export class RoominglistComponent implements OnInit {

  inhouseitem = new Map<any, ReservationLine>();

  constructor(
    private inhouseservices: InhouseService,
    private router: Router) { }

  ngOnInit() {
    this.getAllInhouse();
  }

  homeAction() {
    this.router.navigate(['home']);
  }

  toolAction() {
    this.router.navigate(['tools/tooldashbord']);
  }

  getAllInhouse() {
    this.inhouseservices.getAllInhouse().subscribe(
      (result) => {
        console.log(result);
        result.forEach(element => {
          this.inhouseitem.set(element.saFoReservationLineId, element);
        });
        return;
      }
    );
  }
}
