import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { RoominglistComponent } from './roominglist/roominglist.component';
import { RoominglistRouterModule } from './roominglist-router.module';


@NgModule({
  declarations: [
    RoominglistComponent
  ],
  imports: [
    CommonModule,
    RoominglistRouterModule,
    Ng2CarouselamosModule
  ]
})
export class RoominglistModule { }
