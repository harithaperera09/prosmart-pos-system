import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { CashoutComponent } from './cashout/cashout.component';
import { CashoutRouterModule } from './cashout-router.module';


@NgModule({
  declarations: [
    CashoutComponent
  ],
  imports: [
    CommonModule,
    CashoutRouterModule,
    Ng2CarouselamosModule
  ]
})
export class CashoutModule { }
