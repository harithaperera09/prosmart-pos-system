import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FavouritemainComponent } from './favouritemain/favouritemain.component';


const routes: Routes = [
  {
    path: '',
    component: FavouritemainComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports: [RouterModule]
})

export class FavourIteMainRouterModule { }
