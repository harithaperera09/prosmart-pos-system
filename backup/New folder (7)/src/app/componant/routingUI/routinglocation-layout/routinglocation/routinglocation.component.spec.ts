import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutinglocationComponent } from './routinglocation.component';

describe('RoutinglocationComponent', () => {
  let component: RoutinglocationComponent;
  let fixture: ComponentFixture<RoutinglocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutinglocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutinglocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
