import { Component, OnInit, ElementRef } from '@angular/core';
import { Location } from 'src/app/dto/location';
import { LocationService } from 'src/app/service/location.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-routinglocation',
  templateUrl: './routinglocation.component.html',
  styleUrls: ['./routinglocation.component.scss']
})
export class RoutinglocationComponent implements OnInit {


  locations: Array<Location> = [];
  location: Location;

  constructor(
    private locationService: LocationService,
    private dataserviceservice: DataserviceService,
    private elem: ElementRef,
    private router: Router) { }

  ngOnInit() {
    this.getAllActiveLocationByCc();
  }

  getAllActiveLocationByCc(): void {
    this.locationService.getAllActiveLocationByCc().subscribe(
      (result) => {
        this.locations = result;
        console.log(this.locations);
      }
    );
  }


  locationClickAction(location: Location) {
    this.dataserviceservice.changeLocation(location);
    // this.dataserviceservice.changeProjectByLocationAction(location.saLocationId.toString());
    this.router.navigate(['home/routingservicemancomponent']);
  }
}
