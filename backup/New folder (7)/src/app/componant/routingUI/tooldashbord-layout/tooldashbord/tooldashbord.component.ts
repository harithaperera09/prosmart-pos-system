import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tooldashbord',
  templateUrl: './tooldashbord.component.html',
  styleUrls: ['./tooldashbord.component.scss']
})
export class TooldashbordComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

  }

  okAction() {

  }

  homeAction() {
    this.router.navigate(['home']);
  }

  toolAction() {
    this.router.navigate(['tools/tooldashbord']);
  }

  closeAction() {
    this.router.navigate(['home/routinglocationcomponent']);
  }
  reprintAction() {
    this.router.navigate(['tools/reprint']);
  }

  tranferAction() {
    this.router.navigate(['tools/transfer']);
  }

  roominglistAction() {
    this.router.navigate(['tools/roomlist']);
  }

  movetableAction() {
    this.router.navigate(['tools/movetable']);
  }

  billsplitAction() {
    this.router.navigate(['tools/billsplit']);

  }

  createtableAction() {
    this.router.navigate(['tools/createtable']);
  }

  creditecustomerAction() {
    this.router.navigate(['tools/creditcustomer']);
  }

  guestspecialsAction() {
    this.router.navigate(['tools/gustspecials']);
  }

  margebillAction() {
    this.router.navigate(['tools/margebill']);
  }

  settingAction() { }

  cashinAction() {
    this.router.navigate(['tools/cashin']);
  }

  cashoutAction() {
    this.router.navigate(['tools/cashout']);
  }


}
