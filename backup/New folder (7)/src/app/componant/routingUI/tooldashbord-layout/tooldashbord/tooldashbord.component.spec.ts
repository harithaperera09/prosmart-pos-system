import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TooldashbordComponent } from './tooldashbord.component';

describe('TooldashbordComponent', () => {
  let component: TooldashbordComponent;
  let fixture: ComponentFixture<TooldashbordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TooldashbordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TooldashbordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
