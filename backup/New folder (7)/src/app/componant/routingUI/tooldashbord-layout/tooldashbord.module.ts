import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooldashbordComponent } from './tooldashbord/tooldashbord.component';
import { TooldashDordLayoutRouterModule } from './tooldashbord-router.module';

@NgModule({
  declarations: [
    TooldashbordComponent
  ],
  imports: [
    CommonModule,
    TooldashDordLayoutRouterModule,
    // FormsModule

  ]
})
export class ToolDashbordLayoutModule { }
