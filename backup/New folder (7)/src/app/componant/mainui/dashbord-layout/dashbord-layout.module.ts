import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { OrderdetailComponent } from '../orderdetail/orderdetail.component';
import { FunctionsComponent } from '../functions/functions.component';
import { RoutingfunctionsComponent } from '../routingfunctions/routingfunctions.component';
import { DashbordLayoutRouterModule } from './dashbord-layout-router.module';
import { KeypadkeyboardComponent } from '../../key_functions/keypadkeyboard/keypadkeyboard.component';
import { CashinComponent } from '../../controlpanel/cashin/cashin.component';
import { CashoutComponent } from '../../controlpanel/cashout/cashout.component';
import { FormsModule } from '@angular/forms';
import { ChequeComponent } from '../../settlement/cheque/cheque.component';
import { ComplementaryComponent } from '../../settlement/complementary/complementary.component';
import { CreditcardWithloyaltyComponent } from '../../settlement/creditcard-withloyalty/creditcard-withloyalty.component';
import { CreditcardWithoutloyaltyComponent } from '../../settlement/creditcard-withoutloyalty/creditcard-withoutloyalty.component';
import { OthersComponent } from '../../settlement/others/others.component';
import { PayModeComponent } from '../../pay-mode/pay-mode.component';
import { StaffComponent } from '../../staff/staff.component';
import { StaffsComponent } from '../../settlement/staffs/staffs.component';
import { ViewKotComponent } from '../../settlement/view-kot/view-kot.component';
import { VoucherComponent } from '../../settlement/voucher/voucher.component';

@NgModule({
  declarations: [
    DashboardComponent,
    NavbarComponent,
    OrderdetailComponent,
    FunctionsComponent,
    RoutingfunctionsComponent,
    KeypadkeyboardComponent,
    CashinComponent,
    CashoutComponent,
    CashinComponent,
    CashoutComponent,
    ChequeComponent,
    ComplementaryComponent,
    CreditcardWithloyaltyComponent,
    CreditcardWithoutloyaltyComponent,
    OthersComponent,
    PayModeComponent,
    StaffComponent,
    StaffsComponent,
    ViewKotComponent,
    VoucherComponent,
  ],
  imports: [
    CommonModule,
    DashbordLayoutRouterModule,
    FormsModule
  ]
})
export class DashbordLayoutModule { }
