import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingfunctionsComponent } from './routingfunctions.component';

describe('RoutingfunctionsComponent', () => {
  let component: RoutingfunctionsComponent;
  let fixture: ComponentFixture<RoutingfunctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingfunctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingfunctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
