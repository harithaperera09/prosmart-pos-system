import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelAccountComponent } from './hotel-account.component';

describe('HotelAccountComponent', () => {
  let component: HotelAccountComponent;
  let fixture: ComponentFixture<HotelAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
