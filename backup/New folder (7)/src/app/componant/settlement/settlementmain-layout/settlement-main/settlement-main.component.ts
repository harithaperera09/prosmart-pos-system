import { Component, OnInit, ElementRef } from '@angular/core';
import { DataserviceService } from 'src/app/service/dataservice.service';
// import { element } from '@angular/core/src/render3';
import { Location } from 'src/app/dto/location';
import { Settlement } from 'src/app/dto/settlement';
import { SettlementService } from 'src/app/service/settlement.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-settlement-main',
  templateUrl: './settlement-main.component.html',
  styleUrls: ['./settlement-main.component.scss']
})
export class SettlementMainComponent implements OnInit {

  isbuttonaenable = true;

  settlements = new Map<any, Settlement>();
  selectsettlement: Settlement = new Settlement();

  location: Location;

  constructor(
    private router: Router,
    private settlementservice: SettlementService,
    private elementref: ElementRef,
    private dataserviceservice: DataserviceService) {

  }

  ngOnInit() {
    this.dataserviceservice.currentLocation.subscribe(location => {
      this.location = location;
      this.loadAllProceedOrders();
      return;
    });



  }


  loadAllProceedOrders() {
    this.settlements = new Map<any, Settlement>();
    this.settlementservice.loadAllProceedOrders(this.location.saLocationId).subscribe(
      (result) => {
        result.forEach(elementt => {
          this.settlements.set(elementt.saFoReservationLineId, elementt);
        });
        return;
      }
    );
  }


  tableclickAction(iselectsettlementd: Settlement) {
    this.selectsettlement = iselectsettlementd;
    console.log(this.selectsettlement.saPosMasterId);
    this.isbuttonaenable = false;

  }

  BillCancelationAction() {
    if (this.selectsettlement.saPosMasterId !== 0) {
      alert('ok');
    } else {
      alert('please select row');
    }
  }

  PriceLvlAction() {

    if (this.selectsettlement.saPosMasterId !== null) {
      alert('ok');
    } else {
      alert('please select row');
    }
  }

  ViewKOTAction() {
    if (this.selectsettlement.saPosMasterId !== null) {
      alert('ok');
    } else {
      alert('please select row');
    }
  }

  PrintAction() {
    if (this.selectsettlement.saPosMasterId !== undefined) {
      if (this.selectsettlement.status === 1) {
        this.selectsettlement.status = 2;
        console.log(this.selectsettlement);
        this.settlementservice.chnageprintstatus(this.selectsettlement).subscribe(
          (result) => {
            this.loadAllProceedOrders();
            alert('update ok');
          }
        );
      }
    } else {
      alert('please select row');
    }
  }

  SettleAction() {
    this.dataserviceservice.changesettlementCash(this.selectsettlement);
  }

  DiscountAction() {
    if (this.selectsettlement.saPosMasterId !== null) {
      alert('ok');
    } else {
      alert('please select row');
    }
  }

  homeAction() {
    this.router.navigate(['home']);
  }
}
