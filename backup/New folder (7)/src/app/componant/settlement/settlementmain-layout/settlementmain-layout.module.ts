import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettlementMainComponent } from './settlement-main/settlement-main.component';
import { SettlementMainLayoutRouterModule } from './settlementmain-layout-router.module';

@NgModule({
  declarations: [
    SettlementMainComponent
  ],
  imports: [
    CommonModule,
    SettlementMainLayoutRouterModule
  ]
})
export class SettlementMainLayoutModule { }
