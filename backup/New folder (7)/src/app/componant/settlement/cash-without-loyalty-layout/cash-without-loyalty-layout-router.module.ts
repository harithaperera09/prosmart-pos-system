import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CashWithoutLoyalityComponent } from './cash-without-loyality/cash-without-loyality.component';
const routes: Routes = [
  {
    path: '',
    component: CashWithoutLoyalityComponent,
    children: [

    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CashWithoutLoyaltyLayoutRouterModule { }
