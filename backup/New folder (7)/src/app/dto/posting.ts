export class Posting {
  saPostingId: number;
  saClientId: number;
  saOrgId: number;
  isActive: boolean;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  //
  searchKey: String;
  saFoReservationLineId: number;
  saBpartnerId: number;
  saLocationId: number;
  saCurrencyId: number;
  creditAmount: number;
  debitAmount: number;
  saPaymentModeId: number;
  paymentDescription: String;
  //
  tax1: number;
  tax2: number;
  tax3: number;
  tax4: number;
  tax5: number;
  tax6: number;
  tax7: number;
  tax8: number;
  tax9: number;
  tax10: number;
  //
  grossAmount: number;
  rate: number;
  rateSaCurrencyId: number;
  exchangeRate: number;
  saFoInvoiceId: number;
  remark: String;
  saleNumber: String;
  isCanceled: boolean;
  canceledDate: Date;
  cancelApprovedSaUserId: number;
  canceledSaUserId: number;
  canceledSaUserName: String;
  canceledRemark: String;
  //
  discountAmount: number;
  discountRate: number;
  isDiscountAmount: boolean;
  isDiscountRate: boolean;
  //
  discountApprovedDate: Date;
  discountApprovedSaUserId: number;
  discountSaUserId: number;
  discountRemark: String;
  saFoFolioId: number;
  isPosting: boolean;
  isPayment: boolean;
  isDiscount: boolean;
  isRefund: boolean;
  isAdvancePayment: boolean;
  saFoAdvancePaymentId: number;
  settlementType: String;
  invoiceType: String;
  saFoReservationId: number;
  //
  saClientName: String;
  saOrgName: String;
  saFoReservationLineSearchKey: String;
  saFoReservationSearchKey: String;
  saBpartnerSearchKey: String;
  saBpartnerName: String;
  saBpartnerName2: String;
  saLocationName: String;
  saCurrencySearchKey: String;
  saCurrencyName: String;
  saPaymentModeName: String;
  saFoInvoiceSearchKey: String;
  username: String;
  saFoFolioName: String;
  saFoAdvancePaymentSearchKey: String;
  saFoRoomId: number;
  saFoRoomName: String;
  isGrossRoomCharge: boolean;
  isMealCharge: boolean;
  // for get reservation line details
  rlArrivalDate: Date;
  rlDepartureDate: Date;
  rlSaFoRoomId: number;
  rlSaFoReservationLineId: number;
  saFoRoomCapacityId: number;
  saFoMealPlanId: number;
}
