export class ReservationLine {
  saFoReservationLineId: number;
  saClientId: number;
  saOrgId: number;
  isActive: boolean;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  //
  searchKey: String;
  saFoReservationId: number;
  saFoReservationSearchKey: String;
  saFoRoomTypeId: number;
  saFoRoomTypeName: String;
  billingSaFoRoomTypeId: number;
  billingSaFoRoomTypeName: String;
  saFoRoomCapacityId: number;
  saFoRoomCapacityName: String;
  saFoRoomId: number;
  saFoRoomName: String;
  arrivalDate: Date;
  departureDate: Date;
  complimentaryChildCount: number;
  chargeableChildCount: number;
  child6_12Count: number;
  isReservationLeader: boolean;
  saPriceLevelId: number;
  saPriceLevelName: String;
  status: String;
  remark: String;
  clientName: String;
  orgName: String;
  dragId: number;
  dropId: number;
  saFoMealPlanId: number;
  saFoMealPlanName: String;
  saCurrencyId: number;
  saCurrencyName: String;
  rate: number;
  preferedRoom: String;
  discount: number;
  adultCount: number;
  //
  roomLeaderName: String;
  roomLeaderSearchKey: String;
  roomLeaderId: number;
  //
  reservationLeaderName: String;
  reservationLeaderId: number;
  //
  guestName: String;
  customizeRateSaCurrencyId: number;
  customizeRateSaCurrencyName: String;
  customizeRateRate: number;
  mealPlanSearchKey: String;
  capacitySearchKey: String;
  contractCurrencyExchangeRate: number;
  customizeRateCurrencyExchangeRate: number;
}


