
import { Master } from './mater';
import { MasterLine } from './masterline';

export class Proceed {
  master: Master;
  masterlinelist: Array<MasterLine>;
}
