import { FunctionsComponent } from './componant/mainui/functions/functions.component';

import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { FunctionsComponent } from 'src/app/componant/mainui/report-layout/report';

const routes: Routes = [

  {
    path: 'home',
    loadChildren: 'src/app/componant/mainui/dashbord-layout/dashbord-layout.module#DashbordLayoutModule',
  },
  {
    path: 'login',
    loadChildren: 'src/app/componant/mainui/login-layout/login-layout.module#LoginLayoutModule'
  },
  {
    path: 'report',
    loadChildren: 'src/app/componant/mainui/report-layout/report-layout.module#ReportLayoutModule'
  },
  {
    path: 'settlement',
    loadChildren: 'src/app/componant/mainui/settlement-layout/settlement-layout.module#SettlementLayoutModule'
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
