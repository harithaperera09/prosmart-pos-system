import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Item } from 'src/app/dto/item';
import { Location } from 'src/app/dto/location';
import { Serviceman } from 'src/app/dto/serviceman';
import { Table } from 'src/app/dto/table';
import { FavouriteCatorgory } from '../dto/favouritecatorgary';
import { PriceLevel } from '../dto/PriceLevel';
import { Settlement } from '../dto/settlement';
import { ReservationLine } from '../dto/reservationline';

export const MAIN_URL = 'http://192.168.1.5:8084/SaproPMS';

@Injectable()
export class DataserviceService {

  private LocationSource = new BehaviorSubject<Location>(new Location);
  currentLocation = this.LocationSource.asObservable();

  private ServicemenSource = new BehaviorSubject<Serviceman>(new Serviceman);
  currentservicesmen = this.ServicemenSource.asObservable();

  private TableSource = new BehaviorSubject<Table>(new Table);
  currenttable = this.TableSource.asObservable();

  private ItemSource = new BehaviorSubject(Array<Item>());
  currentItem = this.ItemSource.asObservable();

  private OrderDetailsSource = new BehaviorSubject(null);
  currentOrderDetails = this.OrderDetailsSource.asObservable();

  private ChangeFaviriurteSource = new BehaviorSubject<FavouriteCatorgory>(new FavouriteCatorgory);
  currenfavirurite = this.ChangeFaviriurteSource.asObservable();

  private PriceLevelsource = new BehaviorSubject<PriceLevel>(new PriceLevel);
  curentPriceLevel = this.PriceLevelsource.asObservable();

  private sendAllSelectedItemListToFuctionSource = new BehaviorSubject(new Map<any, Item>());
  curentAllSelectedItemListToFuctionn = this.sendAllSelectedItemListToFuctionSource.asObservable();


  private settlemetncashsource = new BehaviorSubject<Settlement>(new Settlement);
  curentsettlementcash = this.settlemetncashsource.asObservable();

  private clearProceedsourcee = new BehaviorSubject<null>(null);
  curentsettclearProceed = this.clearProceedsourcee.asObservable();

  private inhousesource = new BehaviorSubject<ReservationLine>(new ReservationLine);
  currentinhouse = this.inhousesource.asObservable();

  private ChangeByLocationsource = new BehaviorSubject<String>(''); ////
  currentchnagelocation = this.inhousesource.asObservable();

  constructor() { }


  changeProjectByLocationAction(action: String) { ////
    this.ChangeByLocationsource.next(action);
  }

  changeItemList(itemList: Array<Item>) {
    this.ItemSource.next(itemList);
  }


  changeLocation(location: Location) {
    this.LocationSource.next(location);
  }

  changeServicemen(servicesmen: Serviceman) {
    this.ServicemenSource.next(servicesmen);
  }

  changeTable(table: Table) {
    this.TableSource.next(table);
  }

  addOrderDetail(item: Item) {
    this.OrderDetailsSource.next(item);
  }

  changeFaavirurite(favouritecatorgory: FavouriteCatorgory) {
    this.ChangeFaviriurteSource.next(favouritecatorgory);
  }

  changePriceLevel(pricelevel: PriceLevel) {
    this.PriceLevelsource.next(pricelevel);
  }

  changeFunctionItemList(items: Map<any, Item>) {
    this.sendAllSelectedItemListToFuctionSource.next(items);
  }

  changesettlementCash(settlement: Settlement) {
    this.settlemetncashsource.next(settlement);
  }


  clearProceedAction() {
    this.clearProceedsourcee.next(null);
  }

  changeInhouse(recevation: ReservationLine) {
    this.inhousesource.next(recevation);
  }

}
