import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { MAIN_URL } from '../service/dataservice.service';
import { Settlement } from '../dto/settlement';
import { MasterLine } from './../dto/masterline';

@Injectable()
export class BillSplitService {

  constructor(private http: HttpClient) { }

  loadAllProceedTableOrders(): Observable<Array<Settlement>> {
    return this.http.get<Array<Settlement>>(MAIN_URL + '/proceedAllOrderssByTable');
  }

  loadAllProceedItems(saPosMasterId): Observable<Array<MasterLine>> {
    return this.http.get<Array<MasterLine>>(MAIN_URL + '/serachBillSplitByMasterId-' + saPosMasterId);
  }


}
