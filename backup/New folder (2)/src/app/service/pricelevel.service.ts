import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { PriceLevel } from '../dto/PriceLevel';
import { MAIN_URL } from '../service/dataservice.service';

@Injectable()
export class PriceLevelService {

  constructor(private http: HttpClient) { }


  getAllPriceLevelForPos(): Observable<Array<PriceLevel>> {
    return this.http.get<Array<PriceLevel>>(MAIN_URL + '/getAllPriceLevelsForPos');
  }
}
