import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Location } from '../dto/location';
import { MAIN_URL } from '../service/dataservice.service';

@Injectable()
export class LocationService {

  constructor(private http: HttpClient) { }



  // addTable(customer: Table): Observable<boolean> {
  //   return this.http.post<boolean>(MAIN_URL + URL, customer);
  // }

  // editeTable(customer: Table): Observable<boolean> {
  //   return this.http.post<boolean>(MAIN_URL + URL, customer);
  // }

  // deleteTable(id: string): Observable<boolean> {
  //   return this.http.delete<boolean>(MAIN_URL + URL + '/' + id);
  // }

  // deactivateTable(id: string): Observable<boolean> {
  //   return this.http.delete<boolean>(MAIN_URL + URL + '/' + id);
  // }

  // getTable(id: String): Observable<Table> {
  //   return this.http.get<Table>(MAIN_URL + URL + '/' + id);
  // }

  // getAllTables(): Observable<Array<Table>> {
  //   return this.http.get<Array<Table>>(MAIN_URL + URL);
  // }

  getAllActiveLocationByCc(): Observable<Array<Location>> {
    return this.http.get<Array<Location>>(MAIN_URL + '/getAllActiveLocationByType');
  }


  // getAllActiveTables(): Observable<Array<Table>> {
  //   return this.http.get<Array<Table>>(MAIN_URL + URL);
  // }
}
