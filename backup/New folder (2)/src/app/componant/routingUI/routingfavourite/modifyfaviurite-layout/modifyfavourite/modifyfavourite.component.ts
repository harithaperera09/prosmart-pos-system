import { Component, OnInit, ElementRef } from '@angular/core';
import $ from 'jquery';
import { FavouriteCatorgoryService } from 'src/app/service/favouritecatorgary.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { FavouriteCatorgory } from 'src/app/dto/favouritecatorgary';
import { Location } from 'src/app/dto/location';



@Component({
  selector: 'app-modifyfavourite',
  templateUrl: './modifyfavourite.component.html',
  styleUrls: ['./modifyfavourite.component.scss']
})
export class ModifyfavouriteComponent implements OnInit {


  location: Location;
  allcatogaries: Array<FavouriteCatorgory> = [];
  favouritecatogaryis: FavouriteCatorgory;



  constructor(
    private favouritecatorgaryservice: FavouriteCatorgoryService,
    private elem: ElementRef,
    private dataserviceservice: DataserviceService
  ) { }

  ngOnInit() {
    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);
    this.getAllFavouriteCatorgary();

  }

  getAllFavouriteCatorgary(): void {
    this.favouritecatorgaryservice.getAllFavouriteCatorgary(this.location.saLocationId).subscribe(
      (result) => {
        this.allcatogaries = result;
        console.log(this.allcatogaries);
      }
    );
  }


  favoriteAClickAction(davouritecatorgory: FavouriteCatorgory) {
    this.dataserviceservice.changeFaavirurite(davouritecatorgory);
  }

  plusButtonAction() {
    this.favouritecatogaryis = new FavouriteCatorgory();
    this.favouritecatogaryis.name = 'getAllItems';
    this.dataserviceservice.changeFaavirurite(this.favouritecatogaryis);
  }


}
