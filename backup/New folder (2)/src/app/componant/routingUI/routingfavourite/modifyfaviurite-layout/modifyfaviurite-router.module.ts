import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ModifyfavouriteComponent } from './modifyfavourite/modifyfavourite.component';


const routes: Routes = [
  {
    path : '',
    component : ModifyfavouriteComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class ModifyfaviuriteRouterModule { }
