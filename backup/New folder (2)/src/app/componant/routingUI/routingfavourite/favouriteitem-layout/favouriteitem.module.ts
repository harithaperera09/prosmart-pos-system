import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { FavouriteitemRouterModule } from './favouriteitem-router.module';
import { FavouriteitemsComponent } from './favouriteitems/favouriteitems.component';


@NgModule({
  declarations: [
    FavouriteitemsComponent,
  ],
  imports: [
    CommonModule,
    FavouriteitemRouterModule,
    Ng2CarouselamosModule
  ]
})
export class FavouriteitemModule { }
