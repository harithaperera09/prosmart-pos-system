
import { Component, OnInit, ElementRef } from '@angular/core';
import { Serviceman } from 'src/app/dto/serviceman';
import { ServicemanService } from 'src/app/service/serviceman.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import * as utils from 'src/app/componant/mainui/navbar/navbar.component';
import { Location } from 'src/app/dto/location';
import { Router } from '@angular/router';

@Component({
  selector: 'app-routingserviceman',
  templateUrl: './routingserviceman.component.html',
  styleUrls: ['./routingserviceman.component.scss']
})
export class RoutingservicemanComponent implements OnInit {


  locationId = 1;
  servicemans: Array<Serviceman> = [];
  serviceman: Serviceman;
  location: Location;


  constructor(
    private servicemanservice: ServicemanService,
    private dataserviceservice: DataserviceService,
    private elem: ElementRef,
    private router: Router) { }

  ngOnInit() {
    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);

    this.getAllActiveTableByLocationStatus();



  }

  getAllActiveTableByLocationStatus(): void {
    if (this.location.saLocationId === undefined || this.location.saLocationId === null) {
      alert('Select Location');
      this.router.navigate(['home/routinglocationcomponent']);
    } else {
      this.servicemanservice.getAllBPartner(this.location.saLocationId).subscribe(
        (result) => {
          this.servicemans = result;
          console.log(result);
        }
      );
    }
  }

  servicesmenClickAction(servicemen: Serviceman) {
    if (this.location.saLocationId === undefined) {
      alert('Please Select Location');
      this.router.navigate(['home/routinglocationcomponent']);
    } else {
      this.dataserviceservice.changeServicemen(servicemen);
      this.router.navigate(['home/routingtablecomponent']);
    }


  }

}
