import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingsearchComponent } from './routingsearch.component';

describe('RoutingsearchComponent', () => {
  let component: RoutingsearchComponent;
  let fixture: ComponentFixture<RoutingsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingsearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
