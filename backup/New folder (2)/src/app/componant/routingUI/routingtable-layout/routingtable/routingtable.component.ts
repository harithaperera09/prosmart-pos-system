import { Component, OnInit, ElementRef } from '@angular/core';
import { Table } from 'src/app/dto/table';
import { TableService } from 'src/app/service/table.service';
import * as utils from 'src/app/componant/mainui/navbar/navbar.component';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Serviceman } from 'src/app/dto/serviceman';
import { Location } from 'src/app/dto/location';
import { Router } from '@angular/router';
import { ReservationLine } from 'src/app/dto/reservationline';

@Component({
  selector: 'app-routingtable',
  templateUrl: './routingtable.component.html',
  styleUrls: ['./routingtable.component.scss']
})

export class RoutingtableComponent implements OnInit {


  alltables: Array<Table> = [];
  tablee: Table;
  serviceman: Serviceman;
  location: Location;

  /// tempreservation
  reservationline: ReservationLine;


  constructor(
    private tableService: TableService,
    private dataserviceservice: DataserviceService,
    private elem: ElementRef,
    private router: Router) { }

  ngOnInit() {
    this.dataserviceservice.currentservicesmen.subscribe(servicesmen => this.serviceman = servicesmen);
    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);
    this.getAllActiveTableByLocationStatus();


  }

  getAllActiveTableByLocationStatus(): void {

    if (this.location.saLocationId === undefined || this.location.saLocationId === null) {
      alert('Select Location');
      this.router.navigate(['home/routinglocationcomponent']);
    } else if (this.serviceman.saBpartnerId === undefined || this.serviceman.saBpartnerId === null) {
      alert('Select ServicesMen');
      this.router.navigate(['home/routingservicemancomponent']);
    } else {
      this.tableService.getAllActiveTableByLocationStatus(this.location.saLocationId).subscribe(
        (result) => {
          this.alltables = result;
          console.log(this.alltables);
        }
      );
    }
  }

  tableclickAction(table: Table): void {
    if (this.serviceman.saBpartnerId === undefined) {
      alert('Please Select Servicesmen');
    } else if (this.location.saLocationId === undefined) {
      alert('Please Select Location');
    } else {
      this.dataserviceservice.changeTable(table);

    }
  }

  outsideAction() {

    if (this.location.saLocationId === undefined) {
      alert('Please Select Location');
      this.router.navigate(['home/routinglocationcomponent']);
    } else if (this.serviceman.saBpartnerId === undefined) {
      alert('Please Select Servicesmen');
    } else {
      this.reservationline = new ReservationLine();
      this.reservationline.remark = 'Out Side';
      this.dataserviceservice.changeInhouse(this.reservationline);
      this.router.navigate(['home/routingitemcomponent']);
    }
  }
}


