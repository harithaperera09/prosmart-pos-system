import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingpricelevelComponent } from './routingpricelevel.component';

describe('RoutingpricelevelComponent', () => {
  let component: RoutingpricelevelComponent;
  let fixture: ComponentFixture<RoutingpricelevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingpricelevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingpricelevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
