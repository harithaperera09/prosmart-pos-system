import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutinglocationRouterModule } from './routinglocation-router.module';
import { RoutinglocationComponent } from './routinglocation/routinglocation.component';


@NgModule({
  declarations: [RoutinglocationComponent],
  imports: [
    CommonModule,
    RoutinglocationRouterModule
  ]
})
export class RoutinglocationModule { }
