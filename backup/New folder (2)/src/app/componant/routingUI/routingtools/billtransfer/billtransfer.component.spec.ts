import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilltransferComponent } from './billtransfer.component';

describe('BilltransferComponent', () => {
  let component: BilltransferComponent;
  let fixture: ComponentFixture<BilltransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilltransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilltransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
