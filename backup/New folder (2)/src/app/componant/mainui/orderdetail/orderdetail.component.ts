import { Component, OnInit, ElementRef } from '@angular/core';
import $ from 'jquery';
import { Item } from 'src/app/dto/item';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { ItemserviceService } from 'src/app/service/itemservice.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-orderdetail',
  templateUrl: './orderdetail.component.html',
  styleUrls: ['./orderdetail.component.scss']
})
export class OrderdetailComponent implements OnInit {

  itmKey: String;
  itemqty: number;
  selectitem: Item;
  items = new Map<any, Item>();
  tempitem: Item;
  tempitem2: Item = new Item();

  totalamount: number;


  constructor(
    private itemservice: ItemserviceService,
    private data: DataserviceService
  ) { }




  ngOnInit() {

    this.data.curentsettclearProceed.subscribe(a => {
      this.removeAllorderDetails();
    });


    this.data.currentOrderDetails.subscribe(itemss => {

      if (itemss !== null) {
        if (this.items.size === 0) {
          this.items.set(itemss.searchKey, itemss);
          this.sendTotalAmoutItemListToFunction();
        } else {
          if (this.items.has(itemss.searchKey)) {
            this.tempitem = this.items.get(itemss.searchKey);
            this.tempitem.qty += 1;
            this.tempitem.amount = this.tempitem.qty * itemss.selling_pice;
            this.sendTotalAmoutItemListToFunction();
          } else {
            this.items.set(itemss.searchKey, itemss);
            this.sendTotalAmoutItemListToFunction();
          }
        }
      } else {

      }
    });
  }


  removeAllorderDetails() {
    this.items.forEach(values => {
      this.tempitem = this.items.get(values.searchKey);
      this.tempitem.qty = 1;
      this.tempitem.amount = this.tempitem.selling_pice;
      this.sendTotalAmoutItemListToFunction();
    });

    this.tempitem = new Item();
    this.selectitem = new Item();
    this.items = new Map<any, Item>();
    this.sendTotalAmoutItemListToFunction();
  }


  removeselectedItem(searchKey: number) {
    if (this.items.has(searchKey)) {
      this.tempitem = this.items.get(searchKey);
      // this.tempitem = this.items.get(this.selectitem.searchKey);

      this.tempitem.qty = 1;
      this.tempitem.amount = this.tempitem.selling_pice;

      this.items.delete(searchKey);
      this.sendTotalAmoutItemListToFunction();
    }
  }

  changeValues() {
    this.itemqty = $('#code').val();
    if (this.items.has(this.selectitem.searchKey)) {
      this.tempitem = this.items.get(this.selectitem.searchKey);
      // this.tempitem.qty = Number(this.tempitem.qty) + Number(this.itemqty);
      this.tempitem.qty = Number(this.itemqty);
      this.tempitem.amount = this.tempitem.qty * this.selectitem.selling_pice;
      this.sendTotalAmoutItemListToFunction();
    }
  }

  changitemqty(item: Item, searchkey: number) {
    this.selectitem = this.items.get(searchkey);
  }

  sendTotalAmoutItemListToFunction() {
    this.data.changeFunctionItemList(this.items);

  }


}
