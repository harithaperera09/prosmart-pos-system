import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { OrderdetailComponent } from '../orderdetail/orderdetail.component';
import { FunctionsComponent } from '../functions/functions.component';
import { RoutingfunctionsComponent } from '../routingfunctions/routingfunctions.component';
import { DashbordLayoutRouterModule } from './dashbord-layout-router.module';
import { CatagoryComponent } from '../catagory/catagory.component';
import { KeypadkeyboardComponent } from '../../key_functions/keypadkeyboard/keypadkeyboard.component';
import { RoutingpricelevelComponent } from '../../routingUI/routingpricelevel-layout/routingpricelevel/routingpricelevel.component';
import { BillCancelationComponent } from '../../settlement/bill-cancelation/bill-cancelation.component';
import { BilltransferComponent } from '../../routingUI/routingtools/billtransfer/billtransfer.component';
import { CashinComponent } from '../../controlpanel/cashin/cashin.component';
import { CashoutComponent } from '../../controlpanel/cashout/cashout.component';
import { MergebillComponent } from '../../routingUI/routingtools/mergebill/mergebill.component';
import { ReprintComponent } from '../../routingUI/routingtools/reprint/reprint.component';
import { FormsModule } from '@angular/forms';
import { BillSplitComponent } from '../../routingUI/routingtools/bill-split/bill-split.component';
import { CreatetableComponent } from '../../routingUI/routingtools/createtable/createtable.component';
import { GuestspecialsComponent } from '../../routingUI/routingtools/guestspecials/guestspecials.component';
import { CreditcustomerComponent } from '../../routingUI/routingtools/creditcustomer/creditcustomer.component';
import { MovetableComponent } from '../../routingUI/routingtools/movetable/movetable.component';
import { RoominglistComponent } from '../../routingUI/routingtools/roominglist/roominglist.component';
import { CashWithoutLoyalityComponent } from '../../settlement/cash-without-loyality/cash-without-loyality.component';
import { ChequeComponent } from '../../settlement/cheque/cheque.component';
import { ComplementaryComponent } from '../../settlement/complementary/complementary.component';
import { CreditcardWithloyaltyComponent } from '../../settlement/creditcard-withloyalty/creditcard-withloyalty.component';
import { CreditcardWithoutloyaltyComponent } from '../../settlement/creditcard-withoutloyalty/creditcard-withoutloyalty.component';
import { HotelAccountComponent } from '../../settlement/hotel-account/hotel-account.component';
import { OthersComponent } from '../../settlement/others/others.component';
import { OwnerAccountComponent } from '../../settlement/owner-account/owner-account.component';
import { PayModeComponent } from '../../pay-mode/pay-mode.component';
import { StaffComponent } from '../../staff/staff.component';
import { StaffsComponent } from '../../settlement/staffs/staffs.component';
import { ViewKotComponent } from '../../settlement/view-kot/view-kot.component';
import { VoucherComponent } from '../../settlement/voucher/voucher.component';

@NgModule({
  declarations: [
    DashboardComponent,
    NavbarComponent,
    OrderdetailComponent,
    FunctionsComponent,
    RoutingfunctionsComponent,
    KeypadkeyboardComponent,
    BillCancelationComponent,
    BilltransferComponent,
    CashinComponent,
    MergebillComponent,
    CashoutComponent,
    ReprintComponent,
    BillSplitComponent,
    BilltransferComponent,
    CashinComponent,
    CreatetableComponent,
    CashoutComponent,
    CreditcustomerComponent,
    GuestspecialsComponent,
    MergebillComponent,
    MovetableComponent,
    RoominglistComponent,
    BillCancelationComponent,
    CashWithoutLoyalityComponent,
    ChequeComponent,
    ComplementaryComponent,
    CreditcardWithloyaltyComponent,
    CreditcardWithoutloyaltyComponent,
    HotelAccountComponent,
    OthersComponent,
    OwnerAccountComponent,
    PayModeComponent,
    StaffComponent,
    StaffsComponent,
    ViewKotComponent,
    VoucherComponent,
    // RoutingpricelevelComponent
  ],
  imports: [
    CommonModule,
    DashbordLayoutRouterModule,
    FormsModule
  ]
})
export class DashbordLayoutModule { }
