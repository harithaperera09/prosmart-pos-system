import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettlementComponent } from './settlement/settlement.component';
const routes: Routes = [
  {
    path: '',
    component: SettlementComponent,
    children: [
      {
        path: 'settlementmain',
        loadChildren: 'src/app/componant/settlement/settlementmain-layout/settlementmain-layout.module#SettlementMainLayoutModule'
      },

      {
        path: 'settlementtype',
        loadChildren: 'src/app/componant/settlement/settlement-type-layout/settlementtype-layout.module#SettlementTypeLayoutModule'
      },
      {
        path: 'settlemencash',
        loadChildren: 'src/app/componant/settlement/cash-layout/cash-layout.module#CashLayoutModule'
      },
      {
        path: 'discount',
        loadChildren: 'src/app/componant/settlement/discount-layout/discount-layout.module#DiscountLayoutModule'
      },
    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SettlementLayoutRouterModule { }
