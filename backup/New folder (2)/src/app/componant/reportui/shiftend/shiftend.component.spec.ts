import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftendComponent } from './shiftend.component';

describe('ShiftendComponent', () => {
  let component: ShiftendComponent;
  let fixture: ComponentFixture<ShiftendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
