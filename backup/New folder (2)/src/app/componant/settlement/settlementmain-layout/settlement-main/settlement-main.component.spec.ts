import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettlementMainComponent } from './settlement-main.component';

describe('SettlementMainComponent', () => {
  let component: SettlementMainComponent;
  let fixture: ComponentFixture<SettlementMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettlementMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
