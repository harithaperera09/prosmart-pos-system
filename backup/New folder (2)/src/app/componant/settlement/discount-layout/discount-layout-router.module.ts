import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiscountComponent } from './discount/discount.component';
const routes: Routes = [
  {
    path: '',
    component: DiscountComponent,
    children: [

    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DiscountayoutRouterModule { }
