import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CashComponent } from './cash/cash.component';
const routes: Routes = [
  {
    path: '',
    component: CashComponent,
    children: [

    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CashLayoutRouterModule { }
