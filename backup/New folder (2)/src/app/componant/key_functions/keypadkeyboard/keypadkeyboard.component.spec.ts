import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeypadkeyboardComponent } from './keypadkeyboard.component';

describe('KeypadkeyboardComponent', () => {
  let component: KeypadkeyboardComponent;
  let fixture: ComponentFixture<KeypadkeyboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeypadkeyboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeypadkeyboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
