import { TestBed } from '@angular/core/testing';

import { BillSplitService } from './bill-split.service';

describe('BillSplitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BillSplitService = TestBed.get(BillSplitService);
    expect(service).toBeTruthy();
  });
});
