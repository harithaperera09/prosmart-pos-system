import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { PaymentMode } from '../dto/PaymentMode';
import { MAIN_URL } from '../service/dataservice.service';
import { Settlement } from '../dto/settlement';
import { Posting } from '../dto/posting';


@Injectable()
export class SettlementService {


  constructor(private http: HttpClient) { }


  getAllSettlementType(): Observable<Array<PaymentMode>> {
    return this.http.get<Array<PaymentMode>>(MAIN_URL + '/getallsettlementtype');
  }

  loadAllProceedOrders(locationid): Observable<Array<Settlement>> {
    return this.http.get<Array<Settlement>>(MAIN_URL + '/proceedAllOrderss-' + locationid);
  }

  chnageprintstatus(settlement: Settlement): Observable<number> {
    return this.http.post<number>(MAIN_URL + '/chnageprintstatus', settlement);
  }

  chnagePosMastertstatus(settlement: Settlement): Observable<number> {
    return this.http.post<number>(MAIN_URL + '/chnageposmastertstatus', settlement);
  }


  addPosting(posting: Posting): Observable<number> {
    return this.http.post<number>(MAIN_URL + '/addproceedposting', posting);
  }


}
