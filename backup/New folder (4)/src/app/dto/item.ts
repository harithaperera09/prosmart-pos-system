export class Item {
  saItemMasterId: number;
  saClientId: number;
  saOrgId: number;
  isActive: boolean;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  searchKey: String;  //
  name: String;  //
  description: String;
  remark: String;
  isstocked: boolean;
  ispurchased: boolean;
  issold: boolean;
  isbom: boolean;
  isdiscontinued: boolean;
  discontinuedBy: number;
  printName: String;
  saCategoryId: number;
  isThisBaseItem: boolean;
  baseSaItemMasterId: number;
  imagePath: String;
  guaranteeDays: number;
  saInvAttributesetinstanceId: number;
  saItemTypeId: number;
  scrap: number;
  printStatus: String;


  clientName: String;
  orgName: String;
  categoryName: String;
  baseItemMasterName: String;
  attributesetinstanceName: String;
  itemTypeName: String;


  item_image: String;  //
  sub_category_id: String;
  sub_category_name: String; //
  sub_category_image: String; //
  main_category_id: String; //
  main_category_name: String; //
  main_category_image: String; //
  selling_pice: number;

  // for order details
  qty: number;
  amount: number;
}
