import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { FavouriteItemTableService } from 'src/app/service/favouriteitemtable.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Item } from 'src/app/dto/item';
import { ItemserviceService } from 'src/app/service/itemservice.service';
import { Location } from './../../../../dto/location';
import { FavouriteItem } from 'src/app/dto/FavoriteItem';
import { FavouriteCatorgory } from 'src/app/dto/favouritecatorgary';
import { Master } from 'src/app/dto/mater';
import { Router } from '@angular/router';
import { MasterFavourite } from 'src/app/dto/masterfavourite';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-addfavourite',
  templateUrl: './addfavourite.component.html',
  styleUrls: ['./addfavourite.component.scss']

})
export class AddfavouriteComponent implements OnInit {


  items = new Map<any, Item>();
  // selectedItems = new Map<any, Item>();
  location: Location;


  favouritecatorgory: FavouriteCatorgory = new FavouriteCatorgory();
  selectedItems: Array<Item> = [];
  favouriteMaster = new MasterFavourite();
  seletedfavouritecatorgory: FavouriteCatorgory = new FavouriteCatorgory();

  constructor(
    private _route: ActivatedRoute,
    private itemservice: ItemserviceService,
    private favouriteitemtableservice: FavouriteItemTableService,
    private dataserviceservice: DataserviceService,
    private router: Router,
  ) { }

  categoryname: String;



  ngOnInit() {
    this.categoryname = '';
    document.getElementById('add').style.display = 'none';
    document.getElementById('update').style.display = 'none';
    document.getElementById('addmore').style.display = 'none';
    document.getElementById('remove').style.display = 'none';


    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);

    this.dataserviceservice.currenfavirurite.subscribe(davouritecatorgory => {
      if (davouritecatorgory.name !== undefined || davouritecatorgory.saPosFavoriteCategoryId !== undefined) {
        if (davouritecatorgory.name === 'getAllItems') {
          this.getAllActiveTableByLocationStatus();
          this.categoryname = '';
          document.getElementById('add').style.display = 'block';
          document.getElementById('update').style.display = 'none';
          document.getElementById('addmore').style.display = 'none';
          document.getElementById('remove').style.display = 'none';
          return;
        } else {
          this.seletedfavouritecatorgory = davouritecatorgory;
          this.getAllItemByFavouriteId(davouritecatorgory.saPosFavoriteCategoryId);
          this.categoryname = this.seletedfavouritecatorgory.name;
          document.getElementById('add').style.display = 'none';
          document.getElementById('update').style.display = 'block';
          document.getElementById('addmore').style.display = 'block';
          document.getElementById('remove').style.display = 'block';
        }
      }
    });

    // this.getAllActiveTableByLocationStatus();

    $(document).ready(function () {
      // tslint:disable-next-line:prefer-const
      let activeSystemClass = $('.list-group-item.active');
      // something is entered in search form
      $('#system-search').keyup(function () {
        const that = this;
        // affect all table rows on in systems table
        // tslint:disable-next-line:prefer-const
        let tableBody = $('.table-list-search tbody');
        const tableRowsClass = $('.table-list-search tbody tr');
        $('.search-sf').remove();
        tableRowsClass.each(function (i, val) {
          // Lower text for case insensitive
          const rowText = $(val).text().toLowerCase();
          const inputText = $(that).val().toLowerCase();
          if (inputText !== '') {
            $('.search-query-sf').remove();
            tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Searching for: "'
              + $(that).val()
              + '"</strong></td></tr>');
          } else {
            $('.search-query-sf').remove();
          }

          if (rowText.indexOf(inputText) === -1) {
            // hide rows
            tableRowsClass.eq(i).hide();
          } else {
            $('.search-sf').remove();
            tableRowsClass.eq(i).show();
          }
        });
        // all tr elements are hidden
        if (tableRowsClass.children(':visible').length === 0) {
          tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">No entries found.</td></tr>');
        }
      });
    });
  }

  getAllItemByFavouriteId(avouriteitID) {
    this.items = new Map<any, Item>();
    this.favouriteitemtableservice.getAllfavouriteitembYiD(avouriteitID).subscribe(
      (result) => {
        result.forEach(elementt => {
          elementt.remark = 'selected';
          this.items.set(elementt.saItemMasterId, elementt);
        });
        return;
      }
    );
  }

  getAllActiveTableByLocationStatus(): void {
    if (this.location.saLocationId === undefined || this.location.saLocationId == null) {
      // alert('select location');
      this.router.navigate(['/home/routinglocationcomponent']);
    } else {
      this.items = new Map<any, Item>();
      this.itemservice.getAllActiveItemMasterByLocation(1).subscribe(
        (result) => {
          result.forEach(elementt => {
            elementt.remark = 'unselected';
            this.items.set(elementt.saItemMasterId, elementt);
          });
          return;
        }
      );
    }
  }


  SelectItemAction(saItemMasterId) {
    this.items.forEach(selectelement => {
      if (selectelement.saItemMasterId === saItemMasterId) {
        if (selectelement.remark === 'selected') {
          selectelement.remark = 'unselected';
        } else {
          selectelement.remark = 'selected';
        }
      }
    });
    return;
  }


  addnewCategoryAction() {
    if (this.categoryname === null || this.categoryname === undefined || this.categoryname === '') {
      alert('Insert Favourite Name');
    } else {
      this.items.forEach(addelement => {
        if (addelement.remark === 'selected') {
          addelement.imagePath = 'null';
          addelement.sub_category_image = 'null';
          addelement.main_category_image = 'null';
          this.selectedItems.push(addelement);
        }
      });


      // favouritecatorgory
      this.favouritecatorgory.saPosFavoriteCategoryId = 0;
      this.favouritecatorgory.saClientId = 0;
      this.favouritecatorgory.saOrgId = 0;
      this.favouritecatorgory.isActive = false;
      // this.favouritecatorgory.created =;
      this.favouritecatorgory.createdBy = 0;
      // this.favouritecatorgory.updated: Date;
      this.favouritecatorgory.updatedBy = 0;
      this.favouritecatorgory.searchKey = 'null';
      this.favouritecatorgory.name = this.categoryname;
      this.favouritecatorgory.remark = 'null';
      this.favouritecatorgory.sortOrder = 0;
      this.favouritecatorgory.clientName = 'null';
      this.favouritecatorgory.orgName = 'null';
      this.favouritecatorgory.saLocationId = this.location.saLocationId;


      this.favouriteMaster.favouritecatorgory = this.favouritecatorgory;
      this.favouriteMaster.favouriteItemList = this.selectedItems;
      this.favouriteitemtableservice.addnewFavourite(this.favouriteMaster).subscribe(
        (result) => {
          alert(result);
        }
      );
    }

  }

  updateCategorAction() {
    this.items.forEach(updateelement => {
      if (updateelement.remark === 'selected') {
        this.selectedItems.push(updateelement);
      }
    });

    this.favouritecatorgory.name = this.categoryname;
    this.favouritecatorgory.saLocationId = this.location.saLocationId;
    this.favouriteMaster.favouritecatorgory = this.favouritecatorgory;
    this.favouriteMaster.favouriteItemList = this.selectedItems;
    this.favouriteitemtableservice.UpdateFavourite(this.favouriteMaster).subscribe(
      (result) => {
        alert(result);
      }
    );
  }

  addMoreItemAction() {

    if (this.location.saLocationId === undefined || this.location.saLocationId == null) {
      alert('select location');
      this.router.navigate(['/home/routinglocationcomponent']);
    } else {
      // this.items = new Map<any, Item>();
      this.itemservice.getAllActiveItemMasterByLocation(1).subscribe(
        (result) => {
          result.forEach(elementt => {
            if (!this.items.has(elementt.saItemMasterId)) {
              elementt.remark = 'unselected';
              this.items.set(elementt.saItemMasterId, elementt);
            }
          });
          return;
        }
      );
    }
  }

  deleteAction() {
    this.favouriteitemtableservice.deleteFavouriteCatogary(this.seletedfavouritecatorgory).subscribe(
      (result) => {
        return;
      }
    );

  }

}
