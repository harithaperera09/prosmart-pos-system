import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RoutingsearchComponent } from './routingsearch/routingsearch.component';


const routes: Routes = [
  {
    path : '',
    component : RoutingsearchComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class RoutingsearchRouterModule { }
