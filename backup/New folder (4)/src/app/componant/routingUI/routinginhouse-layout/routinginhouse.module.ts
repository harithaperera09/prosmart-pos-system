import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingInhouseRouterModule } from './routinginhouse-router.module';
import { InhouseComponent } from './inhouse/inhouse.component';


@NgModule({
  declarations: [InhouseComponent],
  imports: [
    CommonModule,
    RoutingInhouseRouterModule
  ]
})
export class RoutingInhouseModule { }
