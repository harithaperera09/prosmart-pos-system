import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingpricelevelComponent } from './routingpricelevel/routingpricelevel.component';
import { RoutingpricelevelRouterModule } from './routingpricelevel-router.module';


@NgModule({
  declarations: [RoutingpricelevelComponent],
  imports: [
    CommonModule,
    RoutingpricelevelRouterModule
  ]
})
export class RoutingpricelevelModule { }
