import { Component, OnInit, ElementRef } from '@angular/core';

import { PriceLevelService } from 'src/app/service/pricelevel.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { PriceLevel } from 'src/app/dto/PriceLevel';

@Component({
  selector: 'app-routingpricelevel',
  templateUrl: './routingpricelevel.component.html',
  styleUrls: ['./routingpricelevel.component.scss']
})
export class RoutingpricelevelComponent implements OnInit {

  allpricelevel: Array<PriceLevel> = [];
  pricelevelies: PriceLevel;

  constructor(
    private pricelevelservice: PriceLevelService,
    private elem: ElementRef,
    private dataserviceservice: DataserviceService
  ) { }

  ngOnInit() {
    this.getAllPriceLevelForPos();

  }

  getAllPriceLevelForPos(): void {
    this.pricelevelservice.getAllPriceLevelForPos().subscribe(
      (result) => {
        this.allpricelevel = result;
        console.log(this.allpricelevel);
      }
    );
  }

  pricelevelchngeAction(pricelevel: PriceLevel) {
    this.dataserviceservice.changePriceLevel(pricelevel);
  }

}
