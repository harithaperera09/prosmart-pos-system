import { Component, OnInit } from '@angular/core';
import { ReservationLine } from 'src/app/dto/reservationline';
import { InhouseService } from 'src/app/service/inhouse.service';

@Component({
  selector: 'app-roominglist',
  templateUrl: './roominglist.component.html',
  styleUrls: ['./roominglist.component.scss']
})
export class RoominglistComponent implements OnInit {

  inhouseitem = new Map<any, ReservationLine>();

  constructor(private inhouseservices: InhouseService) { }

  ngOnInit() {
    this.getAllInhouse();
  }

  getAllInhouse() {
    this.inhouseservices.getAllInhouse().subscribe(
      (result) => {
        console.log(result);
        result.forEach(element => {
          this.inhouseitem.set(element.saFoReservationLineId, element);
        });
        return;
      }
    );
  }
}
