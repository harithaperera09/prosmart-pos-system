import { Component, OnInit } from '@angular/core';
import { SettlementService } from 'src/app/service/settlement.service';
import { PaymentMode } from 'src/app/dto/PaymentMode';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settlement-type',
  templateUrl: './settlement-type.component.html',
  styleUrls: ['./settlement-type.component.scss']
})
export class SettlementTypeComponent implements OnInit {

  settlementtypes = new Map<any, PaymentMode>();

  constructor(
    private settlementservices: SettlementService,
    private router: Router) { }

  ngOnInit() {
    this.getAllSettlementType();
  }

  settlementcash() {
    this.router.navigate(['/settlement/settlemencash']);
  }

  getAllSettlementType() {
    // alert('ok');
    this.settlementservices.getAllSettlementType().subscribe(
      (result) => {
        console.log(result);
        result.forEach(element => {
          this.settlementtypes.set(element.saPaymentModeId, element);
        });
        return;
      }
    );
    console.log(this.settlementtypes);
  }
}
