import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillCancelationComponent } from './bill-cancelation.component';

describe('BillCancelationComponent', () => {
  let component: BillCancelationComponent;
  let fixture: ComponentFixture<BillCancelationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillCancelationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillCancelationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
