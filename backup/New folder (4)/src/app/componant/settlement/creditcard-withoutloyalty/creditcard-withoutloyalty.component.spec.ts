import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditcardWithoutloyaltyComponent } from './creditcard-withoutloyalty.component';

describe('CreditcardWithoutloyaltyComponent', () => {
  let component: CreditcardWithoutloyaltyComponent;
  let fixture: ComponentFixture<CreditcardWithoutloyaltyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditcardWithoutloyaltyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditcardWithoutloyaltyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
