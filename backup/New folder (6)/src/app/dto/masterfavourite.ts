import { FavouriteCatorgory } from './favouritecatorgary';
import { Item } from './item';

export class MasterFavourite {
  favouritecatorgory: FavouriteCatorgory;
  favouriteItemList: Array<Item>;
}
