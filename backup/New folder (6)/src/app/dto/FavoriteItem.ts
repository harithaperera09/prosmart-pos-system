export class FavouriteItem {
    saPosFavoriteId: number;
    saClientId: number;
    saOrgId: number;
    isActive: boolean;
    created: Date;
    createdBy: number;
    updated: Date;
    updatedBy: number;
    searchKey: String;
    name: String;
    remark: String;
    saPosFavoriteCategoryId: number;
    saItemMasterId: number;
    saLocationId: number;
    //
    clientName: String;
    orgName: String;
    posFavoriteCategoryName: String;
    itemMasterName: String;
    locationName: String;
}
