export class PriceLevel {
    saPriceLevelId: number;
    saClientId: number;
    saOrgId: number;
    isActive: boolean;
    created: Date;
    createdBy: number;
    updated: Date;
    updatedBy: number;
    searchKey: String;
    name: String;
    remark: String;

    saClientName: String;
    // tslint:disable-next-line:no-unused-expression
    saOrgName: String;
}
