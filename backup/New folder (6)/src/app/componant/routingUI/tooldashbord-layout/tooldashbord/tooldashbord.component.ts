import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tooldashbord',
  templateUrl: './tooldashbord.component.html',
  styleUrls: ['./tooldashbord.component.scss']
})
export class TooldashbordComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

  }

  okAction() {

  }

  closeAction() {
    this.router.navigate(['home/routinglocationcomponent']);
  }


}
