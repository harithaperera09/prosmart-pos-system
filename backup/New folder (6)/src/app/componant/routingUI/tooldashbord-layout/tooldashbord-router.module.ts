import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TooldashbordComponent } from './tooldashbord/tooldashbord.component';
const routes: Routes = [
  {
    path: '',
    component: TooldashbordComponent,
    children: [
      {
        path: 'settlementmain',
        loadChildren: 'src/app/componant/settlement/settlementmain-layout/settlementmain-layout.module#SettlementMainLayoutModule'
      },

    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TooldashDordLayoutRouterModule { }
