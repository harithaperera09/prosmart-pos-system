import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Item } from 'src/app/dto/item';
import { ItemserviceService } from 'src/app/service/itemservice.service';
import * as utils from 'src/app/componant/mainui/navbar/navbar.component';
import { Majorcategory } from 'src/app/dto/majorcategory';
import { Subcategory } from 'src/app/dto/subcategory';
import { CatagoryComponent } from 'src/app/componant/mainui/catagory/catagory.component';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { forEach } from '@angular/router/src/utils/collection';
import { Serviceman } from 'src/app/dto/serviceman';
import { Table } from 'src/app/dto/table';
import { Location } from 'src/app/dto/location';
import { Router } from '@angular/router';


@Component({
  selector: 'app-routingitmes',
  templateUrl: './routingitmes.component.html',
  styleUrls: ['./routingitmes.component.scss']
})
export class RoutingitmesComponent implements OnInit {


  items: Array<Item> = [];
  tempItem: Item = new Item();

  serviceman: Serviceman;
  table: Table;
  location: Location;

  constructor(private itemservice: ItemserviceService,
    private data: DataserviceService,
    private elem: ElementRef,
    private router: Router) { }

  ngOnInit() {
    this.data.currentItem.subscribe(items => this.items = items);
    this.data.currentservicesmen.subscribe(servicesmen => this.serviceman = servicesmen);
    this.data.currentLocation.subscribe(location => this.location = location);
    this.data.currenttable.subscribe(table => this.table = table);
  }


  selectItemAction(item: Item) {
    if (this.location.saLocationId === undefined) {
      alert('Please Select Location');
      this.router.navigate(['home/routinglocationcomponent']);
    } else if (this.serviceman.saBpartnerId === undefined) {
      alert('Please Select Servicesmen');
      this.router.navigate(['home/routingservicemancomponent']);
    } else if (this.table.name === undefined) {
      alert('Please Select Table');
      this.router.navigate(['home/routingtablecomponent']);
    } else {
      if (item.qty === undefined) {
        item.qty = 1;
        item.amount = item.selling_pice;
        this.data.addOrderDetail(item);
      } else {
        this.data.addOrderDetail(item);
      }
    }
  }

}






