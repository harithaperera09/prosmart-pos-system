import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { ModifyfavouriteComponent } from './modifyfavourite/modifyfavourite.component';
import { ModifyfaviuriteRouterModule } from './modifyfaviurite-router.module';
import { AddfavouriteComponent } from '../addfavourite/addfavourite.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ModifyfavouriteComponent,
    AddfavouriteComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ModifyfaviuriteRouterModule,
    Ng2CarouselamosModule
  ]
})
export class ModifyfaviuriteModule { }
