import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouriteitemsComponent } from './favouriteitems.component';

describe('FavouriteitemsComponent', () => {
  let component: FavouriteitemsComponent;
  let fixture: ComponentFixture<FavouriteitemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouriteitemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteitemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
