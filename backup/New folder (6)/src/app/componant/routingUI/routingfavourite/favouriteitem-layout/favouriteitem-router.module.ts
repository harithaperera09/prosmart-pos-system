import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FavouriteitemsComponent } from './favouriteitems/favouriteitems.component';


const routes: Routes = [
  {
    path : '',
    component : FavouriteitemsComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class FavouriteitemRouterModule { }
