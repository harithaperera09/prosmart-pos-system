import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RoutingpricelevelComponent } from './routingpricelevel/routingpricelevel.component';


const routes: Routes = [
  {
    path : '',
    component : RoutingpricelevelComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class RoutingpricelevelRouterModule { }
