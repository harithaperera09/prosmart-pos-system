import { Component, OnInit } from '@angular/core';
import { BilltransferService } from 'src/app/service/billtransfer.service';
import { ReservationLine } from 'src/app/dto/reservationline';
import { Master } from 'src/app/dto/mater';

@Component({
  selector: 'app-billtransfer',
  templateUrl: './billtransfer.component.html',
  styleUrls: ['./billtransfer.component.scss']
})
export class BilltransferComponent implements OnInit {

  constructor(private billtransferservice: BilltransferService) { }

  list1: Map<String, ReservationLine> = new Map<String, ReservationLine>();
  list2: Map<String, ReservationLine> = new Map<String, ReservationLine>();

  seletedpaymentitemsToTable = new Map<any, Master>();
  seletedrow: Master;
  setTransfertoId: number;
  isTransferTo: boolean;





  ngOnInit() {
    this.GetAllOpenBillPrintRoomId();
  }

  GetAllOpenBillPrintRoomId() {
    this.billtransferservice.GetAllOpenBillPrintRoomId().subscribe(
      (result) => {
        result.forEach(allelement => {
          this.list1.set(allelement.saFoRoomId.toString(), allelement);
          this.list2.set(allelement.saFoRoomId.toString(), allelement);
        });
        return;
      }
    );
  }

  setTransferID(value) {
    this.setTransfertoId = value;
  }

  TransferAction() {

    if (this.setTransfertoId.toString() === undefined || this.setTransfertoId.toString() === '' || this.setTransfertoId === null) {
      alert('Select Transfer To');
    } if (this.seletedrow.toString() === undefined || this.seletedrow.toString() === '' || this.seletedrow === null) {
      alert('Select Table Row');
    } else {
      this.seletedrow.saFoRoomId = this.setTransfertoId;
      this.billtransferservice.setPosMasterRoomId(this.seletedrow).subscribe(
        (result) => {
          alert('ok');
          return;
        }
      );
    }

  }

  tableClick(proceed: Master) {
    alert('table select ok');
    this.seletedrow = proceed;
  }

  getOrderDetailsByRoomID(value) {

    if (value !== null || value !== undefined) {
      this.list2 = new Map<any, ReservationLine>();
      this.billtransferservice.GetAllOpenBillPrintRoomId().subscribe(
        (result) => {
          result.forEach(elementt => {
            if (elementt.saFoRoomId.toString() !== value.toString()) {
              this.list2.set(elementt.saFoRoomId.toString(), elementt);
            }
            return;
          });
        }
      );

      this.list2.delete(value.toString());
      this.seletedpaymentitemsToTable = new Map<any, Master>();
      this.billtransferservice.getAllProceedRoomBillsByRoomId(value).subscribe(
        (result) => {
          result.forEach(element => {
            this.seletedpaymentitemsToTable.set(element.saPosMasterId, element);
          });
          return;
        }
      );
    }

  }

  resetAction() {
    this.list1 = new Map<String, ReservationLine>();
    this.list2 = new Map<String, ReservationLine>();
    this.seletedpaymentitemsToTable = new Map<any, Master>();
    this.setTransfertoId = 0;
    this.isTransferTo = false;
    this.GetAllOpenBillPrintRoomId();
  }

}
