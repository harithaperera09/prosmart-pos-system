import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestspecialsComponent } from './guestspecials.component';

describe('GuestspecialsComponent', () => {
  let component: GuestspecialsComponent;
  let fixture: ComponentFixture<GuestspecialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestspecialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestspecialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
