import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettlementLayoutRouterModule } from './settlement-layout-router.module';
import { SettlementComponent } from './settlement/settlement.component';
import { SettlementnavComponent } from '../navbar/othernav/settlementnav/settlementnav.component';

@NgModule({
  declarations: [
    SettlementComponent,
    SettlementnavComponent,
  ],
  imports: [
    CommonModule,
    SettlementLayoutRouterModule,
    // FormsModule

  ]
})
export class SettlementLayoutModule { }
