import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWithoutLoyalityComponent } from './cash-without-loyality.component';

describe('CashWithoutLoyalityComponent', () => {
  let component: CashWithoutLoyalityComponent;
  let fixture: ComponentFixture<CashWithoutLoyalityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashWithoutLoyalityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashWithoutLoyalityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
