import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { } from 'src/app/componant/routingUI/routingpricelevel-layout/routingpricelevel/routingpricelevel.component';
import { SettlementTypeComponent } from './settlement-type/settlement-type.component';

const routes: Routes = [
  {
    path: '',
    component: SettlementTypeComponent,
    children: [
      {
        path: 'routingsearchcomponent',
        loadChildren: 'src/app/componant/routingUI/routingsearch-layout/routingsearch.module#RoutingsearchModule'
      },
      {
        path: 'routinglocationcomponent',
        loadChildren: 'src/app/componant/routingUI/routinglocation-layout/routinglocation.module#RoutinglocationModule'
      },
      {
        path: 'routingservicemancomponent',
        loadChildren: 'src/app/componant/routingUI/routingserviceman-layout/routingserviceman.module#RoutingservicemanModule'
      },
      {
        path: 'routingtablecomponent',
        loadChildren: 'src/app/componant/routingUI/routingtable-layout/routingtable.module#RoutingtableModule'
      },
      {
        path: 'routingpricelevelcomponent',
        loadChildren: 'src/app/componant/routingUI/routingpricelevel-layout/routingpricelevel.module#RoutingpricelevelModule'
      },
      {
        path: 'routingitemcomponent',
        loadChildren: 'src/app/componant/routingUI/routingitem-layout/routingitmes.module#RoutingitmesModule'
      },
      {
        path: 'favouriteitem',
        loadChildren: 'src/app/componant/routingUI/routingfavourite/favouriteitem-layout/favouriteitem.module#FavouriteitemModule'
      },
      {
        path: 'modifyfaviurite',
        loadChildren: 'src/app/componant/routingUI/routingfavourite/modifyfaviurite-layout/modifyfaviurite.module#ModifyfaviuriteModule'
      }


    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SettlementTypeLayoutRouterModule { }
