import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettlementTypeComponent } from './settlement-type.component';

describe('SettlementTypeComponent', () => {
  let component: SettlementTypeComponent;
  let fixture: ComponentFixture<SettlementTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettlementTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
