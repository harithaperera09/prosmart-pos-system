import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { MAIN_URL } from './dataservice.service';
import { MasterLine } from '../dto/masterline';
import { Proceed } from '../dto/Proceed';
const URL = '';

@Injectable()

export class ProceedService {

  constructor(private http: HttpClient) { }

  proceedOrder(proceed): Observable<boolean> {
    return this.http.put<boolean>(MAIN_URL + '/proceedorder', proceed);
  }


}
