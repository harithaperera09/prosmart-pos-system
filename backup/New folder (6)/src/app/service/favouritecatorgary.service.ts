import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { FavouriteCatorgory } from '../dto/favouritecatorgary';
import { MAIN_URL } from '../service/dataservice.service';


@Injectable()
export class FavouriteCatorgoryService {

  constructor(private http: HttpClient) { }

  getAllFavouriteCatorgary(locationid): Observable<Array<FavouriteCatorgory>> {
    return this.http.get<Array<FavouriteCatorgory>>(MAIN_URL + '/getAllActivePosFavoriteCategoryByLocationId-' + locationid);

  }





}
