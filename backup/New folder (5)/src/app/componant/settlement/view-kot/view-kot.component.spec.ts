import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewKotComponent } from './view-kot.component';

describe('ViewKotComponent', () => {
  let component: ViewKotComponent;
  let fixture: ComponentFixture<ViewKotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewKotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewKotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
