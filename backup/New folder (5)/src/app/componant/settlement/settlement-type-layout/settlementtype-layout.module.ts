import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettlementTypeComponent } from './settlement-type/settlement-type.component';
import { SettlementTypeLayoutRouterModule } from './settlementtype-layout-router.module';

@NgModule({
  declarations: [
    SettlementTypeComponent
  ],
  imports: [
    CommonModule,
    SettlementTypeLayoutRouterModule
  ]
})
export class SettlementTypeLayoutModule { }
