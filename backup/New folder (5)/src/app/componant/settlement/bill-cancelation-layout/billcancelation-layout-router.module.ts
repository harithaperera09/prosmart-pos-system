import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillCancelationComponent } from './bill-cancelation/bill-cancelation.component';
const routes: Routes = [
  {
    path: '',
    component: BillCancelationComponent,
    children: [

    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BillCancelationLayoutRouterModule { }
