import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CashComponent } from './cash/cash.component';
import { CashLayoutRouterModule } from './cash-layout-router.module';

@NgModule({
  declarations: [
    CashComponent
  ],
  imports: [
    CommonModule,
    CashLayoutRouterModule,
    FormsModule

  ]
})
export class CashLayoutModule { }
