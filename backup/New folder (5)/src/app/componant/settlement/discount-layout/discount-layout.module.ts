import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DiscountComponent } from './discount/discount.component';
import { DiscountayoutRouterModule } from './discount-layout-router.module';

@NgModule({
  declarations: [
    DiscountComponent
  ],
  imports: [
    CommonModule,
    DiscountayoutRouterModule,
    FormsModule

  ]
})
export class DiscountLayoutModule { }
