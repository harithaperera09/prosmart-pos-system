import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditcardWithloyaltyComponent } from './creditcard-withloyalty.component';

describe('CreditcardWithloyaltyComponent', () => {
  let component: CreditcardWithloyaltyComponent;
  let fixture: ComponentFixture<CreditcardWithloyaltyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditcardWithloyaltyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditcardWithloyaltyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
