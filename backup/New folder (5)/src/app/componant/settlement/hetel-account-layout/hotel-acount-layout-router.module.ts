import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotelAccountComponent } from 'backup/New folder (2)/src/app/componant/settlement/hotel-account/hotel-account.component';
const routes: Routes = [
  {
    path: '',
    component: HotelAccountComponent,
    children: [

    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HotelAcountLayoutRouterModule { }
