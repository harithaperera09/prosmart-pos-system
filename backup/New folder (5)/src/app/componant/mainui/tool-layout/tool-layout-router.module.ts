import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToolComponent } from './tool/tool.component';
const routes: Routes = [
  {
    path: '',
    component: ToolComponent,
    children: [
      {
        path: 'settlementmain',
        loadChildren: 'src/app/componant/settlement/settlementmain-layout/settlementmain-layout.module#SettlementMainLayoutModule'
      },
      {
        path: 'tooldashbord',
        loadChildren: 'src/app/componant/routingUI/tooldashbord-layout/tooldashbord.module#ToolDashbordLayoutModule'
      },

    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ToolLayoutRouterModule { }
