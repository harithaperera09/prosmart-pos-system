import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportnavComponent } from './reportnav.component';

describe('ReportnavComponent', () => {
  let component: ReportnavComponent;
  let fixture: ComponentFixture<ReportnavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportnavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportnavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
