import { Component, OnInit } from '@angular/core';
import { Alert } from 'selenium-webdriver';
import { OrderdetailsService } from 'src/app/service/orderdetails.service';
import { Table } from 'src/app/dto/table';
declare var $: any;
import { Router } from '@angular/router';
import { FavouriteCatorgory } from 'src/app/dto/favouritecatorgary';
import { FavouriteCatorgoryService } from 'src/app/service/favouritecatorgary.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Location } from 'src/app/dto/location';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  allcatogaries: Array<FavouriteCatorgory> = [];
  favouritecatogaryis: FavouriteCatorgory;
  location: Location;
  searchtext = {};

  computername: String;

  constructor(
    private favouritecatorgaryservice: FavouriteCatorgoryService,
    private router: Router,
    private orderdetailsservice: OrderdetailsService,
    private data: DataserviceService,
  ) { }




  ngOnInit() {

    this.data.currentLocation.subscribe(location => this.location = location);

    this.getAllFavouriteCatorgary();
    document.getElementById('settingpenel').style.width = '0%';
    document.getElementById('settingpenel').style.height = '0%';

    document.getElementById('toolspanel').style.width = '0%';
    document.getElementById('toolspanel').style.height = '0%';

    document.getElementById('fvouritepanel').style.width = '0%';
    document.getElementById('fvouritepanel').style.height = '0%';


    $('#popupmaker').hide();
    this.router.navigate(['home/routinglocationcomponent']);

    // this.data.currentLocation.subscribe(location => {
    //   this.location = location;
    //   this.getAllFavouriteCatorgary();
    // });
  }

  closepanel() {
    document.getElementById('settingpenel').style.width = '0%';
    document.getElementById('settingpenel').style.height = '0%';

    document.getElementById('toolspanel').style.width = '0%';
    document.getElementById('toolspanel').style.height = '0%';

    document.getElementById('fvouritepanel').style.width = '0%';
    document.getElementById('fvouritepanel').style.height = '0%';

    $('#make').show();
  }

  showfuction() {
    document.getElementById('settingpenel').style.width = '50%';
    document.getElementById('settingpenel').style.height = '70%';
    $('#make').hide();
  }

  // showtools() {
  //   document.getElementById('toolspanel').style.width = '25%';
  //   document.getElementById('toolspanel').style.height = '50%';

  //   $('#make').hide();
  // }

  showFvourite() {
    document.getElementById('fvouritepanel').style.width = '50%';
    document.getElementById('fvouritepanel').style.height = '55%';

    $('#make').hide();


    if (this.location.saLocationId === undefined || this.location.saLocationId === null) {
      alert('Select Location');
    } else {
      this.getAllFavouriteCatorgary();
    }
  }

  mergeBillAction() {
    // $('#popupmaker').show();
    // $('#popupmaker').html('<app-pos-report></app-pos-report>');
    $('#makee').html('<div id="popupmaker"><app-pos-report></app-pos-report></div>');
  }

  viewReportUi() { }


  printBillAction() {

    this.searchtext['printname'] = 'print1';

    this.orderdetailsservice.printallbill(this.searchtext).subscribe(
      (result) => {
        alert(result);
      }
    );
  }


  getAllFavouriteCatorgary(): void {
    this.favouritecatorgaryservice.getAllFavouriteCatorgary(this.location.saLocationId).subscribe(result => {
      this.allcatogaries = result;
    });
  }

  toolAction() {
    this.router.navigate(['/tools']);
  }
}
