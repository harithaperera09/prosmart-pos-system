import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { InhouseComponent } from './inhouse/inhouse.component';


const routes: Routes = [
  {
    path : '',
    component : InhouseComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class RoutingInhouseRouterModule { }
