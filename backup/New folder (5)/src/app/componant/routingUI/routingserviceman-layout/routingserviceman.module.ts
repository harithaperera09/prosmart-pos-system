import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingservicemanComponent } from './routingserviceman/routingserviceman.component';
import { RoutingservicemanRouterModule } from './routingserviceman-router.module';


@NgModule({
  declarations: [RoutingservicemanComponent],
  imports: [
    CommonModule,
    RoutingservicemanRouterModule
  ]
})
export class RoutingservicemanModule { }
