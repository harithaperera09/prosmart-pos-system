import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { FavouritemainComponent } from './favouritemain/favouritemain.component';
import { FavourIteMainRouterModule } from './favouritemain-router.module';


@NgModule({
  declarations: [
    FavouritemainComponent,
  ],
  imports: [
    CommonModule,
    FavourIteMainRouterModule,
    Ng2CarouselamosModule
  ]
})
export class FavourIteMainModule { }
