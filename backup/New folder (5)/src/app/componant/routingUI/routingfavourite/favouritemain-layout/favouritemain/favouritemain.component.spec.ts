import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouritemainComponent } from './favouritemain.component';

describe('FavouritemainComponent', () => {
  let component: FavouritemainComponent;
  let fixture: ComponentFixture<FavouritemainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouritemainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouritemainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
