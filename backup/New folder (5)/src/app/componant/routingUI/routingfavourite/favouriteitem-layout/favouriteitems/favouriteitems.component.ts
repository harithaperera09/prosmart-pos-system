
import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Item } from 'src/app/dto/item';
import { FavouriteItemTableService } from 'src/app/service/favouriteitemtable.service';
import { ItemserviceService } from 'src/app/service/itemservice.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Serviceman } from 'src/app/dto/serviceman';
import { Table } from 'src/app/dto/table';
import { Location } from 'src/app/dto/location';

@Component({
  selector: 'app-favouriteitems',
  templateUrl: './favouriteitems.component.html',
  styleUrls: ['./favouriteitems.component.scss']
})
export class FavouriteitemsComponent implements OnInit {

  favouritecatorgoryname;
  items = new Map<any, Item>();
  fullpriceofseletedcategory = 0;

  serviceman: Serviceman;
  table: Table;
  location: Location;

  constructor(
    private _route: ActivatedRoute,
    private favouriteitemtableservice: FavouriteItemTableService,
    private itemservice: ItemserviceService,
    private data: DataserviceService,
    private elem: ElementRef
  ) {

  }

  ngOnInit() {
    this.data.currentservicesmen.subscribe(servicesmen => this.serviceman = servicesmen);
    this.data.currentLocation.subscribe(location => this.location = location);
    this.data.currenttable.subscribe(table => this.table = table);

    this.favouritecatorgoryname = this._route.snapshot.paramMap.get('name');
    this.getAllItemByFavouriteId(this.favouritecatorgoryname);
  }

  selectItemAction(item: Item) {
    if (this.location.saLocationId === undefined) {
      alert('Please Select Location');
    } else if (this.serviceman.saBpartnerId === undefined) {
      alert('Please Select Servicesmen');
    } else if (this.table.name === undefined) {
      alert('Please Select Table');
    } else {
      if (item.qty === undefined) {
        item.qty = 1;
        item.amount = item.selling_pice;
        this.data.addOrderDetail(item);
      } else {
        this.data.addOrderDetail(item);
      }
    }
  }

  selectAllItemAction() {
    this.items.forEach(element => {
      if (element.qty === undefined) {
        element.qty = 1;
        element.amount = element.selling_pice;
        this.data.addOrderDetail(element);
      }
      // if (element.saItemMasterId === saItemMasterId) {
      //   if (element.remark === 'selected') {
      //     element.remark = 'unselected';
      //   } else {
      //     element.remark = 'selected';
      //   }
      // }
    });
  }

  getAllItemByFavouriteId(avouriteitID) {
    this.items = new Map<any, Item>();
    this.favouriteitemtableservice.getAllfavouriteitembYiD(avouriteitID).subscribe(
      (result) => {
        result.forEach(elementt => {
          this.fullpriceofseletedcategory = this.fullpriceofseletedcategory + elementt.selling_pice;
          this.items.set(elementt.saItemMasterId, elementt);
        });
        return;
      }
    );
  }


}
