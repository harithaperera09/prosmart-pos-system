import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovetableComponent } from './movetable.component';

describe('MovetableComponent', () => {
  let component: MovetableComponent;
  let fixture: ComponentFixture<MovetableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovetableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
