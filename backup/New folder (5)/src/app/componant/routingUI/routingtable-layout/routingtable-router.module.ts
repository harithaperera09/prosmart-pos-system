import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RoutingtableComponent } from './routingtable/routingtable.component';


const routes: Routes = [
  {
    path : '',
    component : RoutingtableComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class RoutingtableRouterModule { }
