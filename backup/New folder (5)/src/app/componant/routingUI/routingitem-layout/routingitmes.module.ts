import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingitmesRouterModule } from './routingitmes-router.module';
import { RoutingitmesComponent } from './routingitmes/routingitmes.component';
import { CatagoryComponent } from '../../mainui/catagory/catagory.component';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';


@NgModule({
  declarations: [
    RoutingitmesComponent,
    CatagoryComponent
  ],
  imports: [
    CommonModule,
    RoutingitmesRouterModule,
    Ng2CarouselamosModule
  ]
})
export class RoutingitmesModule { }
