import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingitmesComponent } from './routingitmes.component';

describe('RoutingitmesComponent', () => {
  let component: RoutingitmesComponent;
  let fixture: ComponentFixture<RoutingitmesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingitmesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingitmesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
