export class Table {
  saPosTableId: number;
  saClientId: number;
  saOrgId: number;
  isActive: boolean;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  searchKey: String;
  name: String;
  remark: String;
  saPosTableTypeId: number;
  maxpaxCount: number;
  tableIndex: number;
  isTempTable: boolean;
  saClientName: String;
  saOrgName: String;
  status: number;

}



