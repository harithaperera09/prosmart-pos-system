export class BillMergeDto {
  transactionName: String;
  itemCode: String;
  ItemName: String;
  qty: number;
  price: number;
  total: number;
}
