export class FavouriteCatorgory {
  saPosFavoriteCategoryId: number;
  saClientId: number;
  saOrgId: number;
  isActive: boolean;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  searchKey: String;
  name: String;
  remark: String;
  sortOrder: number;

  //
  clientName: String;
  orgName: String;
  saLocationId: number;
}
