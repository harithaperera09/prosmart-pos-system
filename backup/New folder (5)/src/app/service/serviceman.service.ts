import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Serviceman } from '../dto/serviceman';
import { MAIN_URL } from '../service/dataservice.service';



@Injectable()
export class ServicemanService {
  constructor(private http: HttpClient) { }

  // addserviceman(customer: Table): Observable<boolean> {
  //   return this.http.post<boolean>(MAIN_URL + URL, customer);
  // }

  // editeserviceman(customer: Table): Observable<boolean> {
  //   return this.http.post<boolean>(MAIN_URL + URL, customer);
  // }

  // deleteserviceman(id: string): Observable<boolean> {
  //   return this.http.delete<boolean>(MAIN_URL + URL + '/' + id);
  // }

  // deactivateserviceman(id: string): Observable<boolean> {
  //   return this.http.delete<boolean>(MAIN_URL + URL + '/' + id);
  // }

  // getserviceman(id: String): Observable<Table> {
  //   return this.http.get<Table>(MAIN_URL + URL + '/' + id);
  // }

  // getAllserviceman(): Observable<Array<Table>> {
  //   return this.http.get<Array<Table>>(MAIN_URL + URL);
  // }

  getAllBPartner(locationid): Observable<Array<Serviceman>> {
    return this.http.get<Array<Serviceman>>(MAIN_URL + '/getAllServicemanByLocationId-' + locationid);
  }


  // getAllActiveserviceman(): Observable<Array<Table>> {
  //   return this.http.get<Array<Table>>(MAIN_URL + URL);
  // }
}
