import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Majorcategory } from '../dto/majorcategory';

export const MAIN_URL = 'http://localhost:8084';
const URL = '/SaproPMS-1.0.1/api/v1/table';


@Injectable()
export class MajorcategoryService {


  constructor(private http: HttpClient) { }

  // addTable(customer: Table): Observable<boolean> {
  //   return this.http.post<boolean>(MAIN_URL + URL, customer);
  // }

  // editeTable(customer: Table): Observable<boolean> {
  //   return this.http.post<boolean>(MAIN_URL + URL, customer);
  // }

  // deleteTable(id: string): Observable<boolean> {
  //   return this.http.delete<boolean>(MAIN_URL + URL + '/' + id);
  // }

  // deactivateTable(id: string): Observable<boolean> {
  //   return this.http.delete<boolean>(MAIN_URL + URL + '/' + id);
  // }

  // getTable(id: String): Observable<Table> {
  //   return this.http.get<Table>(MAIN_URL + URL + '/' + id);
  // }

  // getAllTables(): Observable<Array<Table>> {
  //   return this.http.get<Array<Table>>(MAIN_URL + URL);
  // }

  getAllActiveMajorCategory(): Observable<Array<Majorcategory>> {
    return this.http.get<Array<Majorcategory>>(MAIN_URL + URL + '/getallactivetablebylocationstatus- ' );
  }


  // getAllActiveTables(): Observable<Array<Table>> {
  //   return this.http.get<Array<Table>>(MAIN_URL + URL);
  // }
}
