
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Table } from '../dto/table';


import { MAIN_URL } from '../service/dataservice.service';

@Injectable()
export class OrderdetailsService {

  constructor(private http: HttpClient) { }


  // printallbill(printid: String, name: String): Observable<String> {
  //   return this.http.get<String>(MAIN_URL + '/printbill/' + printid + '/name/' + name);
  // }



  printallbill(mapp): Observable<String> {
    return this.http.post<String>(MAIN_URL + '/printbill', mapp);
  }

}
