import { TestBed } from '@angular/core/testing';

import { MergebillService } from './mergebill.service';

describe('MergebillService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MergebillService = TestBed.get(MergebillService);
    expect(service).toBeTruthy();
  });
});
