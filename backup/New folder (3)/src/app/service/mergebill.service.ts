import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { MAIN_URL } from '../service/dataservice.service';
import { BillMergeDto } from '../dto/BillMergeDto';


@Injectable()
export class MergebillService {

  constructor(private http: HttpClient) { }

  getAllBillList(): Observable<Array<String>> {
    return this.http.get<Array<String>>(MAIN_URL + '/serchKeys');
  }

  findbillbyID(name: String): Observable<Array<BillMergeDto>> {
    return this.http.get<Array<BillMergeDto>>(MAIN_URL + '/getDataForTable/' + name);
  }

}
