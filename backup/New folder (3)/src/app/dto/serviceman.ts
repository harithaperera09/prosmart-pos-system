export class Serviceman {
    saBpartnerId: number;
    saClientId: number;
    saOrgId: number;
    isActive: boolean;
    created: Date;
    createdBy: number;
    updated: Date;
    updatedBy: number;
    searchKey: String;
    name: String;
    name2: String;
    description: String;
    company: String;
    isEmployee: boolean;
    isGuest: boolean;
    isTourOperator: boolean;
    isRealTourOperator: boolean;
    remark: String;
    saDesignationId: number;
    isDefault: boolean;
    isCustomizeRate: boolean;
    vatRegNo: String;
    email: String;
    phoneNo: String;
    faxNo: String;
    mobileNo: String;
    saFoTitleId: number;
    saFoTitleName: String;
    saCountryId: number;
    saFoNationalityId: String;
    saCountryName: String;
    saFoNationalityName: String;
    birthday: Date;
    saFoProfessionId: number;
    saFoProfessionName: String;
    nicNo: String;
    passportNo: String;
    isCustomer: boolean;
    isSupplier: String;
    isZeroAllow: String;
    isServiceMan: boolean;
    isLoyaltyCustomer: boolean;
}


