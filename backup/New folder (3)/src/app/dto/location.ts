export class Location {
  saLocationId: number;
  saClientId: number;
  saOrgId: number;
  isActive: boolean;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  searchKey: String;
  name: String;
  description: String;
  saLocationTypeId: number;
  remark: String;
  saFoBuildingId: number;
  saFoFloorId: number;
  saAddressId: number;
  saLocationTypeName: String;
  saClientName: String;
  saOrgName: String;
}
