import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OwnerAccountComponent } from './owner-account/owner-account.component';
import { OwnerAcountLayoutRouterModule } from './owner-acount-layout-router.module';

@NgModule({
  declarations: [
    OwnerAccountComponent
  ],
  imports: [
    CommonModule,
    OwnerAcountLayoutRouterModule,
    FormsModule

  ]
})
export class OwnerAcountLayoutModule { }
