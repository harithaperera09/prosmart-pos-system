import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HotelAccountComponent } from 'backup/New folder (2)/src/app/componant/settlement/hotel-account/hotel-account.component';
import { HotelAcountLayoutRouterModule } from './hotel-acount-layout-router.module';

@NgModule({
  declarations: [
    HotelAccountComponent
  ],
  imports: [
    CommonModule,
    HotelAcountLayoutRouterModule,
    FormsModule

  ]
})
export class HotelAcountLayoutModule { }
