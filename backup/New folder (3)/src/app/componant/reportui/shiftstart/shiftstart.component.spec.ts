import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftstartComponent } from './shiftstart.component';

describe('ShiftstartComponent', () => {
  let component: ShiftstartComponent;
  let fixture: ComponentFixture<ShiftstartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftstartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftstartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
