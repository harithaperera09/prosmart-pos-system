import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditcustomerComponent } from './creditcustomer.component';

describe('CreditcustomerComponent', () => {
  let component: CreditcustomerComponent;
  let fixture: ComponentFixture<CreditcustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditcustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditcustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
