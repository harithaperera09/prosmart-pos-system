import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingservicemanComponent } from './routingserviceman.component';

describe('RoutingservicemanComponent', () => {
  let component: RoutingservicemanComponent;
  let fixture: ComponentFixture<RoutingservicemanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingservicemanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingservicemanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
