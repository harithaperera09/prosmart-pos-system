import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RoutingservicemanComponent } from './routingserviceman/routingserviceman.component';


const routes: Routes = [
  {
    path : '',
    component : RoutingservicemanComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports : [RouterModule]
})
export class RoutingservicemanRouterModule { }
