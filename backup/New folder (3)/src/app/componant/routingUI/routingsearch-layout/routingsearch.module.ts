import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingsearchComponent } from './routingsearch/routingsearch.component';
import { RoutingsearchRouterModule } from './routingsearch-router.module';


@NgModule({
  declarations: [RoutingsearchComponent],
  imports: [
    CommonModule,
    RoutingsearchRouterModule
  ]
})
export class RoutingsearchModule { }
