import { Component, OnInit, Inject } from '@angular/core';
import $ from 'jquery';
import { FormsModule } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { Item } from 'src/app/dto/item';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { ItemserviceService } from 'src/app/service/itemservice.service';
import { Serviceman } from 'src/app/dto/serviceman';
import { Table } from 'src/app/dto/table';
import { PriceLevel } from 'src/app/dto/PriceLevel';
import { Location } from 'src/app/dto/location';
import { ProceedService } from './../../../service/proceedservice.service';
import { Router } from '@angular/router';
import { Proceed } from './../../../dto/Proceed';
import { Master } from 'src/app/dto/mater';
import { MasterLine } from './../../../dto/masterline';
import { getTestBed } from '@angular/core/testing';
import { ReservationLine } from 'src/app/dto/reservationline';


@Component({
  selector: 'app-functions',
  templateUrl: './functions.component.html',
  styleUrls: ['./functions.component.scss']
})
export class FunctionsComponent implements OnInit {

  order = new Map<String, any>();

  serviceman: Serviceman;
  table: Table;
  location: Location;
  pricelevel: PriceLevel;
  reservationline: ReservationLine;


  items = new Map<any, Item>();
  totalAmount = 0;

  elem;
  fullscreenaction = 0;
  paxcount: number;

  masterline: MasterLine;
  proceed: Proceed = new Proceed;
  master: Master = new Master;
  masterlinelist: Array<MasterLine> = [];

  constructor(private itemservice: ItemserviceService,
    private data: DataserviceService,
    private proceedService: ProceedService,
    private router: Router,
    @Inject(DOCUMENT) private document: any) { }


  ngOnInit() {
    this.paxcount = 1;

    this.elem = document.documentElement;

    this.data.currentservicesmen.subscribe(servicesmen => this.serviceman = servicesmen);
    this.data.currentLocation.subscribe(location => this.location = location);
    this.data.currenttable.subscribe(table => this.table = table);
    this.data.curentPriceLevel.subscribe(pricelevel => this.pricelevel = pricelevel);

    this.data.currentinhouse.subscribe(reservationline => this.reservationline = reservationline);



    this.data.curentAllSelectedItemListToFuctionn.subscribe(items => {
      this.items = new Map<any, Item>();
      this.totalAmount = 0;
      this.items = items;
      this.items.forEach(element => {
        this.totalAmount = this.totalAmount + element.amount;
      });

    });
  }

  processedAction() {



    if (this.items.size === 0) {
      alert('Fist Select Item For Proceed');
    } else {
      // this.master.saPosMasterId = 0;
      // this.master.saClientId = 0;
      // this.master.saOrgId = 0;
      // this.master.saClientId = 0;
      // this.master.isActive = true;
      // this.master.created = new Date;
      // this.master.createdBy = 0;
      // this.master.updated = new Date;
      // this.master.updatedBy = 0;
      // this.master.searchKey = '';
      // this.master.oldsearchkey = '';
      this.master.saPosTableId = this.table.saPosTableId;
      this.master.saLocationId = this.location.saLocationId;
      this.master.servicemanSaBpartnerId = this.serviceman.saBpartnerId;
      // this.master.guestSaBpartnerId = 0;
      // this.master.saFoReservationLineId = 0; ///

      if (this.reservationline.remark === 'In Side') {
        this.master.saFoRoomId = this.reservationline.saFoRoomId;
      } else {
        this.master.saFoRoomId = 0;
      }

      this.master.saPriceLevelId = this.pricelevel.saPriceLevelId;
      this.master.adultCount = this.paxcount;
      // this.master.isCancel = false;
      // this.master.canceledDate = new Date;
      // this.master.cancelApprovedSaUserId = 0;
      // this.master.canceledSaUserId = 0;
      // this.master.canceledRemark = '';
      // this.master.canceledMachine = ''; ///
      // this.master.createdMachine = ''; //
      this.master.status = 1;
      // this.master.billStartTime = new Date;
      // this.master.billEndTime = new Date;
      // this.master.isDiscountPercent = false;
      // this.master.discountMethod = '';
      // this.master.discountAmount = 0;
      // this.master.discountRemark = '';
      this.master.subTotal = this.totalAmount; //////
      // this.master.billPrintCount = 0;
      // this.master.printMachine = '';
      // this.master.settledMachine = '';
      // this.master.recallStatus = 0;
      // this.master.splitFromSaPosMasterId = 0;
      // this.master.remark = '';

      // this.master.clientName = '';
      // this.master.orgName = '';
      // this.master.posTableName = '';
      // this.master.locationName = '';
      // this.master.servicemanBpartnerName = '';
      // this.master.guestBpartnerName = '';
      // this.master.roomName = '';
      // this.master.priceLevelName = '';
      // this.master.cancelApprovedUserName = '';
      // this.master.canceledUserName = '';




      this.items.forEach(element => {
        this.masterline = new MasterLine();
        // this.masterline.saPosMasterLineId = 0;
        // this.masterline.saClientId = 0;
        // this.masterline.saOrgId = 0;
        // this.masterline.saClientId = 0;
        // this.masterline.isActive = true;
        // this.masterline.created = new Date;
        // this.masterline.createdBy = 0;
        // this.masterline.updated = new Date;
        // this.masterline.updatedBy = 0;
        // this.masterline.searchKey = '';
        // this.masterline.tax1 = 0;
        // this.masterline.tax2 = 0;
        // this.masterline.tax3 = 0;
        // this.masterline.tax4 = 0;
        // this.masterline.tax5 = 0;
        // this.masterline.tax6 = 0;
        // this.masterline.tax7 = 0;
        // this.masterline.tax8 = 0;
        // this.masterline.tax9 = 0;
        // this.masterline.tax10 = 0;
        this.masterline.itemMasterId = element.saItemMasterId;
        this.masterline.quentity = element.qty;
        // this.masterline.orderTicketNo = 0;
        // this.masterline.saPosOrderTicketTypeId = 0;
        this.masterline.unitPrice = element.amount;
        // this.masterline.isDelivered = false;
        // this.masterline.isPriceChanged = false;
        // this.masterline.isOrderTicketPrint = false;
        // this.masterline.isDiscountPercent = false;
        // this.masterline.discountAmount = 0;  /////
        // this.masterline.discountPercent = 0; ///
        // this.masterline.discountRemark = '';  ///
        // this.masterline.isCancel = false;
        // this.masterline.canceledDate = new Date;
        // this.masterline.cancelApprovedSaUserId = 0;
        // this.masterline.canceledSaUserId = 0;
        // this.masterline.canceledRemark = '';
        // this.masterline.canceledMachine = ''; ///
        // this.masterline.createdMachine = ''; ///
        // this.masterline.remark = '';

        // this.masterline.clientName = '';
        // this.masterline.orgName = '';
        // this.masterline.posMasterName = '';
        // this.masterline.posOrderTicketTypeName = '';
        // this.masterline.cancelApprovedUserName = '';
        // this.master.canceledUserName = '';

        this.masterlinelist.push(this.masterline);
      });

      this.proceed.master = this.master;
      this.proceed.masterlinelist = this.masterlinelist;

      // this.order.set('master', this.master);
      // this.order.set('masterline', this.masterline);

      this.proceedService.proceedOrder(this.proceed).subscribe(
        (result) => {
          if (result) {
            alert('proceed sucessfully');
            this.clearProceed();

          } else {
            alert('proceed failed');
          }
        }
      );
    }




  }

  openFullscreen() {
    if (this.fullscreenaction === 0) {
      if (this.elem.requestFullscreen) {
        this.elem.requestFullscreen();
      } else if (this.elem.mozRequestFullScreen) {
        /* Firefox */
        this.elem.mozRequestFullScreen();
      } else if (this.elem.webkitRequestFullscreen) {
        /* Chrome, Safari and Opera */
        this.elem.webkitRequestFullscreen();
      } else if (this.elem.msRequestFullscreen) {
        /* IE/Edge */
        this.elem.msRequestFullscreen();
      }
      this.fullscreenaction = 1;
    } else {
      if (this.document.exitFullscreen) {
        this.document.exitFullscreen();
      } else if (this.document.mozCancelFullScreen) {
        /* Firefox */
        this.document.mozCancelFullScreen();
      } else if (this.document.webkitExitFullscreen) {
        /* Chrome, Safari and Opera */
        this.document.webkitExitFullscreen();
      } else if (this.document.msExitFullscreen) {
        /* IE/Edge */
        this.document.msExitFullscreen();
      }
      this.fullscreenaction = 0;
    }


  }


  clearProceed() {
    this.data.clearProceedAction();
  }


  paxmunis() {
    this.paxcount = this.paxcount - 1;
  }

  paxplus() {
    this.paxcount = this.paxcount + 1;
  }


  ReportAction() {
    this.router.navigate(['/report']);
  }

  resetAction() {

    window.location.reload();

  }

  settlementAction() {
    this.router.navigate(['/settlement']);
  }

}
