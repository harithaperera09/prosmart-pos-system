import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettlementComponent } from './settlement/settlement.component';
const routes: Routes = [
  {
    path: '',
    component: SettlementComponent,
    children: [
      {
        path: 'settlementmain',
        loadChildren: 'src/app/componant/settlement/settlementmain-layout/settlementmain-layout.module#SettlementMainLayoutModule'
      },

      {
        path: 'settlementtype',
        loadChildren: 'src/app/componant/settlement/settlement-type-layout/settlementtype-layout.module#SettlementTypeLayoutModule'
      },
      {
        path: 'settlemencash',
        loadChildren: 'src/app/componant/settlement/cash-layout/cash-layout.module#CashLayoutModule'
      },
      {
        path: 'discount',
        loadChildren: 'src/app/componant/settlement/discount-layout/discount-layout.module#DiscountLayoutModule'
      },
      {
        path: 'hotelaccount',
        loadChildren: 'src/app/componant/settlement/hetel-account-layout/hotel-acount-layout.module#HotelAcountLayoutModule'
      },
      {
        path: 'owneraccount',
        loadChildren: 'src/app/componant/settlement/owner-account-layout/owner-acount-layout.module#OwnerAcountLayoutModule'
      },
      {
        path: 'billcancelation',
        loadChildren: 'src/app/componant/settlement/bill-cancelation-layout/billcancelation-layout.module#BillCancelationLayoutModule'
      },
    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SettlementLayoutRouterModule { }
