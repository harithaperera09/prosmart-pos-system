
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Proceed } from '../dto/Proceed';
import { MAIN_URL } from '../service/dataservice.service';
import { ReservationLine } from '../dto/reservationline';
import { Master } from '../dto/mater';



@Injectable()
export class BilltransferService {

  constructor(private http: HttpClient) { }

  GetAllOpenBillPrintRoomId(): Observable<Array<ReservationLine>> {
    return this.http.get<Array<ReservationLine>>(MAIN_URL + '/GetAllOpenBillPrintRoomId');
  }

  getAllProceedRoomBillsByRoomId(roomid): Observable<Array<Master>> {
    return this.http.get<Array<Master>>(MAIN_URL + '/getAllProceedRoomBillsByRoomId-' + roomid);
  }

  setPosMasterRoomId(proceed: Master): Observable<boolean> {
    return this.http.post<boolean>(MAIN_URL + '/editPosMasterRoomId' , proceed);
  }

}
