import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Proceed } from '../dto/Proceed';
import { MAIN_URL } from '../service/dataservice.service';
import { Table } from 'src/app/dto/table';
import { Master } from '../dto/mater';

@Injectable()
export class MovetableService {


  constructor(private http: HttpClient) { }

  GetAllOpenBillPrintTableId(): Observable<Array<Master>> {
    return this.http.get<Array<Master>>(MAIN_URL + '/GetAllOpenBillPrintTableID');
  }

  getAllProceedTableBillsByRoomId(tableid): Observable<Array<Master>> {
    return this.http.get<Array<Master>>(MAIN_URL + '/getAllProceedTableBillsByTableID-' + tableid);
  }

  setPosMasterTableId(table: Master): Observable<boolean> {
    return this.http.post<boolean>(MAIN_URL + '/moveTableTo', table);
  }
}
