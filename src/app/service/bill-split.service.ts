import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { MAIN_URL } from '../service/dataservice.service';
import { Settlement } from '../dto/settlement';
import { MasterLine } from './../dto/masterline';

@Injectable()
export class BillSplitService {

  constructor(private http: HttpClient) { }

  loadAllProceedTableOrders(salocationID): Observable<Array<Settlement>> {
    return this.http.get<Array<Settlement>>(MAIN_URL + '/proceedAllOrderss-' + salocationID);
  }

  loadAllProceedItems(saPosMasterId): Observable<Array<Settlement>> {
    return this.http.get<Array<Settlement>>(MAIN_URL + '/viewKotByPosmasterID-' + saPosMasterId);
  }

  createnewBill(): Observable<Array<Settlement>> {
    return this.http.get<Array<Settlement>>(MAIN_URL + '/viewKotByPosmasterID-');
  }

  getMasterLineByMasterLineID(saPosMasterId): Observable<Settlement> {
    return this.http.get<Settlement>(MAIN_URL + '/searchMasterLineByItemCode-' + saPosMasterId);
  }



}
