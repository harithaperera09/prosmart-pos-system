import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { FavouriteItem } from '../dto/FavoriteItem';
import { MAIN_URL } from '../service/dataservice.service';
import { Item } from '../dto/item';
import { MasterFavourite } from '../dto/masterfavourite';
import { FavouriteCatorgory } from '../dto/favouritecatorgary';

@Injectable()
export class FavouriteItemTableService {

  constructor(private http: HttpClient) { }

  getAllfavouriteitemtable(): Observable<Array<FavouriteItem>> {
    return this.http.get<Array<FavouriteItem>>(MAIN_URL + '/getAllItembyFavouriteTable');
  }

  getAllfavouriteitembYiD(id: number): Observable<Array<Item>> {
    return this.http.get<Array<Item>>(MAIN_URL + '/getAllItemByFavouriteCatogaryId-' + id);
  }

  addnewFavourite(masterfavourite: MasterFavourite): Observable<boolean> {
    return this.http.post<boolean>(MAIN_URL + '/addnewfavourite', masterfavourite);
  }

  UpdateFavourite(masterfavourite: MasterFavourite): Observable<boolean> {
    return this.http.put<boolean>(MAIN_URL + '/updatenewfavourite', masterfavourite);
  }

  deleteFavouriteCatogary(favouritecatorgory: FavouriteCatorgory): Observable<boolean> {
    return this.http.delete<boolean>(MAIN_URL + '/deletefavourite' + favouritecatorgory);
  }

}
