import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Item } from '../dto/item';
import { MAIN_URL } from '../service/dataservice.service';
import { ReservationLine } from '../dto/reservationline';

@Injectable({
  providedIn: 'root'
})
export class InhouseService {

  constructor(private http: HttpClient) { }


  getAllInhouse(): Observable<Array<ReservationLine>> {
    return this.http.get<Array<ReservationLine>>(MAIN_URL + '/GetAllOpenBillPrintRoomId');
  }

}
