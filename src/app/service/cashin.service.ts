import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Proceed } from '../dto/Proceed';
import { MAIN_URL } from '../service/dataservice.service';
import { CashInOut } from '../dto/CashInOut';
import { Master } from '../dto/mater';


@Injectable()
export class CashinService {

  constructor(private http: HttpClient) { }


  SaveCashIN(cashin: CashInOut): Observable<boolean> {
    return this.http.post<boolean>(MAIN_URL + '/savecashin' , cashin);
  }

}
