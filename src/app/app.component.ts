import { Component, OnInit } from '@angular/core';
import { Notification } from 'src/app/dto/Notification';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/service/dataservice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'SaproPosFE';

  constructor() { }


  ngOnInit(): void {
  }


}

