import { BillCancelationComponent } from './componant/settlement/bill-cancelation-layout/bill-cancelation/bill-cancelation.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableService } from './service/table.service';
import { LocationService } from './service/location.service';
import { ServicemanService } from './service/serviceman.service';
import { MajorcategoryService } from './service/majorcategory.service';
import { ItemserviceService } from './service/itemservice.service';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { OrderdetailsService } from './service/orderdetails.service';
import { PriceLevelService } from './service/pricelevel.service';
import { FavouriteCatorgoryService } from './service/favouritecatorgary.service';
import { FavouriteItemTableService } from './service/favouriteitemtable.service';
import { ProceedService } from './service/proceedservice.service';
import { SettlementService } from './service/settlement.service';
import { BilltransferService } from './service/billtransfer.service';
import { MovetableService } from './service/movetable.service';
import { MergebillService } from './service/mergebill.service';
import { BillSplitService } from './service/bill-split.service';
import { FavouritemainComponent } from './componant/routingUI/routingfavourite/favouritemain-layout/favouritemain/favouritemain.component';
import { ToolComponent } from './componant/mainui/tool-layout/tool/tool.component';
import { TooldashbordComponent } from './componant/routingUI/tooldashbord-layout/tooldashbord/tooldashbord.component';
import { PricelvlComponent } from './componant/settlement/pricelvl-layout/pricelvl/pricelvl.component';
import { CashoutService } from './service/cashout.service';
import { CashinService } from './service/cashin.service';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [

NgbModule.forRoot(),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    Ng2CarouselamosModule

  ],
  providers: [
    TableService,
    LocationService,
    ServicemanService,
    MajorcategoryService,
    ItemserviceService,
    DataserviceService,
    SettlementService,
    OrderdetailsService,
    PriceLevelService,
    FavouriteCatorgoryService,
    FavouriteItemTableService,
    ProceedService,
    BilltransferService,
    MovetableService,
    MergebillService,
    BillSplitService,
    CashinService,
    CashoutService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

