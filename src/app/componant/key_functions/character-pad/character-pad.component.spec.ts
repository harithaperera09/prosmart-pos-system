import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterPadComponent } from './character-pad.component';

describe('CharacterPadComponent', () => {
  let component: CharacterPadComponent;
  let fixture: ComponentFixture<CharacterPadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterPadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterPadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
