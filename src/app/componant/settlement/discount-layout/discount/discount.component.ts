import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.scss']
})
export class DiscountComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  okAction() { }

  closeAction() { }

  homeAction() {
    this.router.navigate(['home']);
  }

  settlementAction() {
    this.router.navigate(['settlement/settlementmain']);
  }


}
