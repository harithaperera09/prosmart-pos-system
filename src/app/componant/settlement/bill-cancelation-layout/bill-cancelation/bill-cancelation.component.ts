import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bill-cancelation',
  templateUrl: './bill-cancelation.component.html',
  styleUrls: ['./bill-cancelation.component.scss']
})
export class BillCancelationComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  homeAction() {
    this.router.navigate(['home']);
  }

  settlementAction() {
    this.router.navigate(['settlement/settlementmain']);
  }

}
