import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BillCancelationComponent } from './bill-cancelation/bill-cancelation.component';
import { BillCancelationLayoutRouterModule } from './billcancelation-layout-router.module';

@NgModule({
  declarations: [
    BillCancelationComponent
  ],
  imports: [
    CommonModule,
    BillCancelationLayoutRouterModule,
    FormsModule

  ]
})
export class BillCancelationLayoutModule { }
