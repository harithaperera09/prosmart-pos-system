import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricelvlComponent } from './pricelvl.component';

describe('PricelvlComponent', () => {
  let component: PricelvlComponent;
  let fixture: ComponentFixture<PricelvlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricelvlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricelvlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
