import { Component, OnInit, ElementRef } from '@angular/core';
import { PriceLevel } from 'src/app/dto/PriceLevel';
import { PriceLevelService } from 'src/app/service/pricelevel.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-pricelvl',
  templateUrl: './pricelvl.component.html',
  styleUrls: ['./pricelvl.component.scss']
})
export class PricelvlComponent implements OnInit {
  allpricelevels: Array<PriceLevel> = [];

  pricelevel: PriceLevel;


  constructor(
    private pricelevelservice: PriceLevelService,
    private elem: ElementRef,
    private dataserviceservice: DataserviceService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getAllPriceLevelForPos();
  }

  homeAction() {
    this.router.navigate(['home']);
  }

  settlementAction() {
    this.router.navigate(['settlement/settlementmain']);
  }

  getAllPriceLevelForPos(): void {
    this.pricelevelservice.getAllPriceLevelForPos().subscribe(
      (result) => {
        console.log(result);
        this.allpricelevels = result;
      }
    );
  }

  selectpricelvel() {

  }

  pricelevelchngeAction(pricelevel: PriceLevel) {
    this.dataserviceservice.changePriceLevel(pricelevel);
  }

}
