import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PricelvlyoutRouterModule } from './pricelvl-layout-router.module';
import { PricelvlComponent } from './pricelvl/pricelvl.component';

@NgModule({
  declarations: [
    PricelvlComponent
  ],
  imports: [
    CommonModule,
    PricelvlyoutRouterModule,
    FormsModule

  ]
})
export class PricelvlLayoutModule { }
