import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PricelvlComponent } from './pricelvl/pricelvl.component';
const routes: Routes = [
  {
    path: '',
    component: PricelvlComponent,
    children: [

    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PricelvlyoutRouterModule { }
