import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Settlement } from 'src/app/dto/settlement';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { SettlementService } from 'src/app/service/settlement.service';
import { BillSplitService } from 'src/app/service/bill-split.service';
import { MasterLine } from 'src/app/dto/masterline';

@Component({
  selector: 'app-view-kot',
  templateUrl: './view-kot.component.html',
  styleUrls: ['./view-kot.component.scss']
})
export class ViewKotComponent implements OnInit {
  selectsettlement: Settlement;
  Proceeditems = new Map<any, Settlement>();

  constructor(
    private router: Router,
    private dataserviceservice: DataserviceService,
    private billSplitservice: BillSplitService) { }

  ngOnInit() {

    this.dataserviceservice.curentsettlementcash.subscribe(settlement => {
      this.selectsettlement = settlement;
    });

    this.billSplitservice.loadAllProceedItems(this.selectsettlement.saPosMasterId).subscribe(
      (result) => {
        result.forEach(elementt => {
          elementt.isTableSelected = false;
          this.Proceeditems.set(elementt.itemMasterId, elementt);
        });
        return;
      }
    );
  }

  homeAction() {
    this.router.navigate(['home']);
  }

  settlementAction() {
    this.router.navigate(['settlement/settlementmain']);
  }

}
