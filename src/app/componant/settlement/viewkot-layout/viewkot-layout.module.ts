import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewKotComponent } from './view-kot/view-kot.component';
import { ViewKotLayoutRouterModule } from './viewkot-layout-router.module';

@NgModule({
  declarations: [
    ViewKotComponent
  ],
  imports: [
    CommonModule,
    ViewKotLayoutRouterModule
  ]
})
export class ViewKotLayoutModule { }
