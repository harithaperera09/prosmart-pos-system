import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { } from 'src/app/componant/routingUI/routingpricelevel-layout/routingpricelevel/routingpricelevel.component';
import { ViewKotComponent } from './view-kot/view-kot.component';

const routes: Routes = [
  {
    path: '',
    component: ViewKotComponent,
    children: [
      {
        path: 'routingsearchcomponent',
        loadChildren: 'src/app/componant/routingUI/routingsearch-layout/routingsearch.module#RoutingsearchModule'
      },
    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ViewKotLayoutRouterModule { }
