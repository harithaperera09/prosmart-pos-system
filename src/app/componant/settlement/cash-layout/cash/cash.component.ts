import { Component, OnInit, ElementRef } from '@angular/core';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Settlement } from 'src/app/dto/settlement';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cash',
  templateUrl: './cash.component.html',
  styleUrls: ['./cash.component.scss']
})
export class CashComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {

  }

  okAction() {

  }

  closeAction() { }

  homeAction() {
    this.router.navigate(['home']);
  }

  settlementAction() {
    this.router.navigate(['settlement/settlementmain']);
  }

}
