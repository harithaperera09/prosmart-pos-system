import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CashWithoutLoyaltyLayoutRouterModule } from './cash-without-loyalty-layout-router.module';
import { CashWithoutLoyalityComponent } from './cash-without-loyality/cash-without-loyality.component';

@NgModule({
  declarations: [
    CashWithoutLoyalityComponent
  ],
  imports: [
    CommonModule,
    CashWithoutLoyaltyLayoutRouterModule,
    FormsModule

  ]
})
export class CashWithoutLoyaltyLayoutModule { }
