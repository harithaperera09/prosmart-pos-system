import { Component, OnInit } from '@angular/core';
import { Settlement } from 'src/app/dto/settlement';
import { Posting } from 'src/app/dto/posting';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { SettlementService } from 'src/app/service/settlement.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cash-without-loyality',
  templateUrl: './cash-without-loyality.component.html',
  styleUrls: ['./cash-without-loyality.component.scss']
})
export class CashWithoutLoyalityComponent implements OnInit {

  selectsettlement: Settlement;
  receive: number;
  balance: number;
  posting: Posting;
  remarks: string;

  constructor(
    private dataserviceservice: DataserviceService,
    private settlementService: SettlementService,
    private router: Router
  ) { }


  ngOnInit() {
    this.dataserviceservice.curentsettlementcash.subscribe(settlement => {
      this.selectsettlement = settlement;


    });

    // this.dataserviceservice.curentsettlementcash.subscribe(servicesmen => this.selectsettlement = servicesmen);
  }

  homeAction() {
    this.router.navigate(['home']);
  }

  settlementAction() {
    this.router.navigate(['settlement/settlementmain']);
  }

  closeAction() {

  }

  balancecalculate() {
    if (this.selectsettlement.subTotal <= this.receive) {
    } else {
      alert('Not Enough Payment');
    }
    this.balance = 0;
    this.balance = this.receive - this.selectsettlement.subTotal;
  }

  CashOkAction() {

    if (this.selectsettlement.subTotal <= this.receive) {
      // Full Payment Add OK
      this.selectsettlement.status = 3;
      this.settlementService.chnagePosMastertstatus(this.selectsettlement).subscribe(
        (result) => {
          alert('Full Payment Add OK');
          this.setPostingAction();
          return;
        }
      );

    } else {
      // Not Enough Payment Add OK
      this.selectsettlement.status = 4;
      this.settlementService.chnagePosMastertstatus(this.selectsettlement).subscribe(
        (result) => {
          alert('Not Enough Payment Add OK');
          this.setPostingAction();
          return;
        }
      );
    }
  }

  setPostingAction() {
    this.posting = new Posting();
    this.posting.saClientId = this.selectsettlement.ClientId;
    this.posting.saOrgId = this.selectsettlement.saOrgId;
    this.posting.isActive = true;
    this.posting.searchKey = this.selectsettlement.saPosMasterId + '';
    this.posting.saLocationId = this.selectsettlement.LocationId;
    this.posting.saCurrencyId = 34;
    this.posting.creditAmount = this.selectsettlement.subTotal;
    this.posting.debitAmount = this.selectsettlement.subTotal;
    this.posting.saPaymentModeId = 11;
    this.posting.paymentDescription = this.remarks;
    this.posting.tax1 = this.selectsettlement.tax1;
    this.posting.tax2 = this.selectsettlement.tax2;
    this.posting.tax3 = this.selectsettlement.tax3;
    this.posting.tax4 = this.selectsettlement.tax4;
    this.posting.tax5 = this.selectsettlement.tax5;
    this.posting.tax6 = this.selectsettlement.tax6;
    this.posting.tax7 = this.selectsettlement.tax7;
    this.posting.tax8 = this.selectsettlement.tax8;
    this.posting.tax9 = this.selectsettlement.tax9;
    this.posting.tax10 = this.selectsettlement.tax10;
    this.posting.rateSaCurrencyId = 34;
    this.posting.created = new Date();
    this.posting.updated = new Date();

    this.settlementService.addPosting(this.posting).subscribe(
      (result) => {
        alert('Posting Add OK');
        return;
      }
    );

  }
}
