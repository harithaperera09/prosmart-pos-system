import { Component, OnInit, ElementRef } from '@angular/core';
import { DataserviceService } from 'src/app/service/dataservice.service';
// import { element } from '@angular/core/src/render3';
import { Location } from 'src/app/dto/location';
import { Settlement } from 'src/app/dto/settlement';
import { SettlementService } from 'src/app/service/settlement.service';
import { Router } from '@angular/router';
import { Notification } from 'src/app/dto/Notification';
declare var $: any;

@Component({
  selector: 'app-settlement-main',
  templateUrl: './settlement-main.component.html',
  styleUrls: ['./settlement-main.component.scss']
})
export class SettlementMainComponent implements OnInit {

  isbuttonaenable = true;

  settlements = new Map<any, Settlement>();
  selectsettlement: Settlement = new Settlement();
  notification: Notification;

  location: Location;

  constructor(
    private router: Router,
    private settlementservice: SettlementService,
    private elementref: ElementRef,
    private dataserviceservice: DataserviceService) {

  }

  ngOnInit() {
    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);

    this.notification = new Notification;
    this.loadAllProceedOrders();

    this.dataserviceservice.currentnotification.subscribe(notification => {
      if (notification.title === 'RefeshSettlementMain') {
        this.loadAllProceedOrders();
      }
    });

  }


  loadAllProceedOrders() {
    this.settlements = new Map<any, Settlement>();
    this.settlementservice.loadAllProceedOrders(this.location.saLocationId).subscribe(
      (result) => {
        console.log(result);
        result.forEach(elementt => {

          if (this.selectsettlement.saPosMasterId === elementt.saPosMasterId) {
            elementt.isTableSelected = true;
            this.settlements.set(elementt.saPosMasterId, elementt);
          } else {
            elementt.isTableSelected = false;
            this.settlements.set(elementt.saPosMasterId, elementt);
          }

        });
        this.notification.title = '';
        this.dataserviceservice.changenotification(this.notification);
        return;
      }
    );
  }


  tableclickAction(iselectsettlementd: Settlement) {

    this.selectsettlement = new Settlement();

    /// select row
    this.settlements.forEach(selectone => {
      if (selectone.saPosMasterId === iselectsettlementd.saPosMasterId) {
        selectone.isTableSelected = true;
      } else {
        selectone.isTableSelected = false;
      }
    });

    this.selectsettlement = iselectsettlementd;
    this.isbuttonaenable = false;

    this.dataserviceservice.changesettlementCash(iselectsettlementd);
  }

  BillCancelationAction() {
    if (this.selectsettlement.saPosMasterId !== 0) {
      alert('ok');
    } else {
      alert('please select row');
    }
  }

  PriceLvlAction() {

    if (this.selectsettlement.saPosMasterId !== null) {
      alert('ok');
    } else {
      alert('please select row');
    }
  }

  ViewKOTAction() {
    if (this.selectsettlement.saPosMasterId !== null) {
      alert('ok');
    } else {
      alert('please select row');
    }
  }

  PrintAction() {
    if (this.selectsettlement.saPosMasterId !== undefined) {
      if (this.selectsettlement.status === 1) {
        this.selectsettlement.status = 2;
        console.log(this.selectsettlement);
        this.settlementservice.chnageprintstatus(this.selectsettlement).subscribe(
          (result) => {
            this.loadAllProceedOrders();
            alert('update ok');
          }
        );
      }
    } else {
      alert('please select row');
    }
  }

  SettleAction() {
    this.dataserviceservice.changesettlementCash(this.selectsettlement);
  }

  DiscountAction() {
    if (this.selectsettlement.saPosMasterId !== null) {
      alert('ok');
    } else {
      alert('please select row');
    }
  }

  homeAction() {
    this.router.navigate(['home']);
  }
}
