import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HotelAcountLayoutRouterModule } from './hotel-acount-layout-router.module';
import { HotelAccountComponent } from './hotel-account/hotel-account.component';

@NgModule({
  declarations: [
    HotelAccountComponent
  ],
  imports: [
    CommonModule,
    HotelAcountLayoutRouterModule,
    FormsModule

  ]
})
export class HotelAcountLayoutModule { }
