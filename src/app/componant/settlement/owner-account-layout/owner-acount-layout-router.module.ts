import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OwnerAccountComponent } from './owner-account/owner-account.component';
const routes: Routes = [
  {
    path: '',
    component: OwnerAccountComponent,
    children: [

    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class OwnerAcountLayoutRouterModule { }
