import { Component, OnInit } from '@angular/core';
import { MovetableService } from 'src/app/service/movetable.service';
import { Table } from 'src/app/dto/table';
import { Proceed } from 'src/app/dto/Proceed';
import { Master } from 'src/app/dto/mater';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movetable',
  templateUrl: './movetable.component.html',
  styleUrls: ['./movetable.component.scss']
})
export class MovetableComponent implements OnInit {

  constructor(
    private movetableservice: MovetableService,
    private router: Router) { }


  tableitems = new Map<any, Master>();
  seletedTableitemsToTable = new Map<any, Master>();
  seletedrow: Master;
  setTransfertoId: number;
  isTransferTo: boolean;





  ngOnInit() {
    this.GetAllOpenBillPrintRoomId();
  }


  homeAction() {
    this.router.navigate(['home']);
  }

  toolAction() {
    this.router.navigate(['tools/tooldashbord']);
  }



  GetAllOpenBillPrintRoomId() {
    this.movetableservice.GetAllOpenBillPrintTableId().subscribe(
      (result) => {
        result.forEach(element => {
          element.isTableSelected = false;
          this.tableitems.set(element.saPosMasterId, element);
        });
        return;
      }
    );
  }

  setTransferID(value) {
    this.setTransfertoId = value;
  }

  TransferAction() {

    if (this.setTransfertoId === undefined || this.setTransfertoId === null) {
      alert('Select Transfer To');
    } if (this.seletedrow === undefined || this.seletedrow === null) {
      alert('Select Table Row');
    } else {
      this.seletedrow.saPosTableId = this.setTransfertoId;

      this.movetableservice.setPosMasterTableId(this.seletedrow).subscribe(
        (result) => {
          alert('ok');
          return;
        }
      );
    }

  }

  tableClick(proceed: Master) {

    // seleted row
    this.tableitems.forEach(element => {
      if (proceed.saPosMasterId === element.saPosMasterId) {
        element.isTableSelected = true;
      } else {
        element.isTableSelected = false;
      }
    });

    this.seletedrow = proceed;
  }

  getOrderDetailsByRoomID(value) {
    this.movetableservice.getAllProceedTableBillsByRoomId(value).subscribe(
      (result) => {
        console.log(result);
        result.forEach(element => {
          this.seletedTableitemsToTable.set(element.saPosMasterId, element);
        });
        return;
      }
    );
  }

  resetAction() {
    this.tableitems = new Map<any, Master>();
    this.seletedTableitemsToTable = new Map<any, Master>();
    this.setTransfertoId = 0;
    this.isTransferTo = false;
    this.GetAllOpenBillPrintRoomId();
  }

}
