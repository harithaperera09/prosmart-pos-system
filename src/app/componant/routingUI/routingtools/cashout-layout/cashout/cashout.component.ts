import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CashinService } from 'src/app/service/cashin.service';
import { CashoutService } from 'src/app/service/cashout.service';
import { CashInOut } from 'src/app/dto/CashInOut';

@Component({
  selector: 'app-cashout',
  templateUrl: './cashout.component.html',
  styleUrls: ['./cashout.component.scss']
})
export class CashoutComponent implements OnInit {

  cashin: CashInOut;

  constructor(
    private router: Router,
    private cashoutservice: CashoutService
  ) { }

  ngOnInit() {
  }


  homeAction() {
    this.router.navigate(['home']);
  }

  toolAction() {
    this.router.navigate(['tools/tooldashbord']);
  }

  saveCashOut() {
    this.cashoutservice.SaveCashOut(this.cashin).subscribe(
      (result) => {
        alert(result);
      }
    );
  }


}
