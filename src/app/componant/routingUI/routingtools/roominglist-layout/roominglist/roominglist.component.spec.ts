import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoominglistComponent } from './roominglist.component';

describe('RoominglistComponent', () => {
  let component: RoominglistComponent;
  let fixture: ComponentFixture<RoominglistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoominglistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoominglistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
