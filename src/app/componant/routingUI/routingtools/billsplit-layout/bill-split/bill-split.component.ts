import { Component, OnInit } from '@angular/core';
import { BillSplitService } from 'src/app/service/bill-split.service';
import { Settlement } from 'src/app/dto/settlement';
import { MasterLine } from 'src/app/dto/masterline';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Location } from 'src/app/dto/location';
import { Notification } from 'src/app/dto/Notification';
declare var $: any;

import { Router } from '@angular/router';
import { concat } from 'rxjs';
@Component({
  selector: 'app-bill-split',
  templateUrl: './bill-split.component.html',
  styleUrls: ['./bill-split.component.scss']
})
export class BillSplitComponent implements OnInit {

  location: Location;

  ProceedTableOrders = new Map<any, Settlement>();
  seletedtableorder = new Settlement();
  Proceeditems = new Map<any, Settlement>();

  temProceeditem = new Settlement();
  temProceeditemmap = new Map<any, Settlement>();

  cantsplit = false;

  notification: Notification;

  // newitemrow

  itemcode: number;
  totalqty1: number;
  totalqty2: number;

  constructor(
    private billSplitservice: BillSplitService,
    private dataserviceservice: DataserviceService,
    private router: Router,
    private data: DataserviceService) { }

  ngOnInit() {
    this.notification = new Notification;
    this.data.currentnotification.subscribe(notification => this.notification = notification);
    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);

    this.loadAllProceedTableOrders();


    $('.tab tr').click(function () {
      $(this).toggleClass('clicked');
    });
  }


  homeAction() {
    this.router.navigate(['home']);
  }

  toolAction() {
    this.router.navigate(['tools/tooldashbord']);
  }

  loadAllProceedTableOrders() {
    this.billSplitservice.loadAllProceedTableOrders(this.location.saLocationId).subscribe(
      (result) => {
        result.forEach(element => {
          element.isTableSelected = false;
          this.ProceedTableOrders.set(element.saPosMasterId, element);
        });
        return;
      }
    );

  }

  tableclickAction(saPosMasterId) {
    alert(saPosMasterId);
    this.temProceeditem = new Settlement();
    this.temProceeditemmap = new Map<any, Settlement>();

    this.itemcode = 0;
    this.totalqty1 = 0;
    this.totalqty2 = 0;

    this.cantsplit = false;

    /// change seleted row
    this.ProceedTableOrders.forEach(seleted => {
      if (seleted.saPosMasterId === saPosMasterId) {
        this.seletedtableorder = seleted;
        seleted.isTableSelected = true;
      } else {
        seleted.isTableSelected = false;
      }
    });

    // alert(saPosMasterId);
    this.Proceeditems = new Map<any, Settlement>();
    this.billSplitservice.loadAllProceedItems(saPosMasterId).subscribe(
      (result) => {
        // alert(result.length);
        result.forEach(elementt => {
          elementt.isTableSelected = false;
          this.Proceeditems.set(elementt.itemMasterId, elementt);

          console.log(result.length);
          if (result.length === 1) {
            // alert(elementt.quentity);
            if (elementt.quentity === 1) {
              this.cantsplit = true;
              // alert('ók');
            }
          }

        });
        return;
      }
    );
  }

  orderdetailsTableAction(masterline: Settlement) {

    alert(masterline.posMasterLineId);

    if (this.cantsplit) {
      alert(this.cantsplit);
      /////// alert
      this.notification.image = 'warning.png';
      this.notification.title = 'Oooops !';
      this.notification.discription = 'Please Select Location For Using Tool';
      this.data.changenotification(this.notification);
      document.getElementById('tempbutton').click();
    }


    /// seleted row
    this.Proceeditems.forEach(seletefrow => {
      if (seletefrow.saPosMasterId === masterline.itemMasterId) {
        seletefrow.isTableSelected = true;
      } else {
        seletefrow.isTableSelected = false;
      }
    });

    masterline.isTableSelected = true;

    this.itemcode = 0;
    this.totalqty1 = 0;
    this.totalqty2 = 0;
    this.itemcode = masterline.itemMasterId;
    this.totalqty1 = masterline.quentity;
    this.totalqty2 = masterline.quentity;


    if (this.totalqty2 === 1) {
      $('#qty2').prop('disabled', true);
    } else {
      $('#qty2').prop('disabled', false);
    }

    return;

  }


  addAction() {
    this.temProceeditem = new Settlement();

    if (this.totalqty1 >= this.totalqty2) {
      if (this.totalqty2 >= 1) {

        this.Proceeditems.forEach(addelement => {

          if (addelement.itemMasterId === this.itemcode) {

            if (this.temProceeditemmap.size === 0) {


              // add to temp itemlist


              alert(addelement.posMasterLineId);
              this.billSplitservice.getMasterLineByMasterLineID(addelement.posMasterLineId).subscribe(
                (result) => {
                  console.log(result);
                  this.temProceeditem = result;
                  // this.temProceeditem.quentity = Number($('#qty11').val());
                  // this.temProceeditem.subTotalWithoutTax = this.temProceeditem.quentity * this.temProceeditem.sellingPrice;
                  this.temProceeditemmap.set(this.temProceeditem.itemMasterId, this.temProceeditem);
                  return;
                }
              );



              /// old order details
              // addelement.quentity = addelement.quentity - this.totalqty2;
              // addelement.subTotalWithoutTax = addelement.quentity * this.temProceeditem.sellingPrice;

            } else {


              this.temProceeditemmap.forEach(temprow => {

                if (temprow.itemMasterId === this.itemcode) {
                  temprow.quentity = temprow.quentity + Number($('#qty11').val());
                  temprow.subTotalWithoutTax = temprow.quentity * temprow.sellingPrice;
                  // this.temProceeditemmap.set(this.temprow.itemMasterId, this.temProceeditem);

                  // / old order details
                  // addelement.quentity = addelement.quentity - this.totalqty2;
                  // addelement.subTotalWithoutTax = addelement.quentity * this.temProceeditem.sellingPrice;




                }
              });
            }



          }

          if (this.totalqty1 === this.totalqty2) {
            this.Proceeditems.delete(addelement.itemMasterId);
          }

        });



        this.temProceeditemmap.forEach(addelementt => {
          this.seletedtableorder.subTotal = this.seletedtableorder.subTotal - addelementt.subTotalWithoutTax;
        });


      } else {
        alert('invalid input');
      }
    } else {
      alert('invalid input');
    }

    this.itemcode = 0;
    this.totalqty1 = 0;
    this.totalqty2 = 0;

    return;
  }


  createnewBill() {

    /// seleted main order
    // this.seletedtableorder

    /// temp orderdetailslist
    // this.temProceeditemmap

    /// now orderdetailslist
    // this.Proceeditems

    this.billSplitservice.createnewBill().subscribe(
      (result) => {
        alert(result);
      }
    );

  }

}
