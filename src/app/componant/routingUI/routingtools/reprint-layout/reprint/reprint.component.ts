import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reprint',
  templateUrl: './reprint.component.html',
  styleUrls: ['./reprint.component.scss']
})
export class ReprintComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }


  homeAction() {
    this.router.navigate(['home']);
  }

  toolAction() {
    this.router.navigate(['tools/tooldashbord']);
  }


}
