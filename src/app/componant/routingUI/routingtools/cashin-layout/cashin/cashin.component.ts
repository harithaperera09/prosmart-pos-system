import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CashInOut } from 'src/app/dto/CashInOut';
import { CashinService } from 'src/app/service/cashin.service';

@Component({
  selector: 'app-cashin',
  templateUrl: './cashin.component.html',
  styleUrls: ['./cashin.component.scss']
})
export class CashinComponent implements OnInit {

  cashin: CashInOut;

  constructor(
    private router: Router,
    private cashinservice: CashinService
  ) { }

  ngOnInit() {
  }


  homeAction() {
    this.router.navigate(['home']);
  }

  toolAction() {
    this.router.navigate(['tools/tooldashbord']);
  }



  saveCashIn() {
    this.cashinservice.SaveCashIN(this.cashin).subscribe(
      (result) => {
        alert(result);
      }
    );
  }



}
