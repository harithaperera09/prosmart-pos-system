import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyfavouriteComponent } from './modifyfavourite.component';

describe('ModifyfavouriteComponent', () => {
  let component: ModifyfavouriteComponent;
  let fixture: ComponentFixture<ModifyfavouriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyfavouriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyfavouriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
