import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FavouriteCatorgory } from 'src/app/dto/favouritecatorgary';
import { FavouriteCatorgoryService } from 'src/app/service/favouritecatorgary.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Location } from 'src/app/dto/location';


@Component({
  selector: 'app-favouritemain',
  templateUrl: './favouritemain.component.html',
  styleUrls: ['./favouritemain.component.scss']
})
export class FavouritemainComponent implements OnInit {

  location: Location;
  allcatogaries: Array<FavouriteCatorgory> = [];

  constructor(
    private favouritecatorgaryservice: FavouriteCatorgoryService,
    private router: Router,
    private data: DataserviceService,
  ) { }

  ngOnInit() {
    this.data.currentLocation.subscribe(location => this.location = location);
    this.getAllFavouriteCatorgary();
  }


  getAllFavouriteCatorgary(): void {
    this.favouritecatorgaryservice.getAllFavouriteCatorgary(this.location.saLocationId).subscribe(result => {
      this.allcatogaries = result;
    });
  }


  modyfyclickAction() {
    this.router.navigate(['/home/modifyfaviurite']);
  }

  clickAction(id) {
    // this.router.navigate(['/home/faviuritemain']);
    this.router.navigate(['/home/favouriteitem', { name: id }]);
  }
}
