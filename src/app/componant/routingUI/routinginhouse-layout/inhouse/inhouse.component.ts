import { Component, OnInit } from '@angular/core';
import { InhouseService } from './../../../../service/inhouse.service';
import { ReservationLine } from 'src/app/dto/reservationline';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Serviceman } from 'src/app/dto/serviceman';
import { Table } from 'src/app/dto/table';
import { Location } from 'src/app/dto/location';

@Component({
  selector: 'app-inhouse',
  templateUrl: './inhouse.component.html',
  styleUrls: ['./inhouse.component.scss']
})
export class InhouseComponent implements OnInit {

  inhouseitem = new Map<any, ReservationLine>();

  serviceman: Serviceman;
  table: Table;
  location: Location;

  constructor(
    private inhouseservices: InhouseService,
    private router: Router,
    private dataserviceservice: DataserviceService
  ) { }

  ngOnInit() {
    this.getAllInhouse();
    // this.dataserviceservice.currentItem.subscribe(items => this.items = items);
    this.dataserviceservice.currentservicesmen.subscribe(servicesmen => this.serviceman = servicesmen);
    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);
    this.dataserviceservice.currenttable.subscribe(table => this.table = table);
  }

  getAllInhouse() {
    this.inhouseservices.getAllInhouse().subscribe(
      (result) => {
        console.log(result);
        result.forEach(element => {
          this.inhouseitem.set(element.saFoReservationLineId, element);
        });
        return;
      }
    );
  }

  inhouseClickAction(reservationlinee: ReservationLine) {
    if (this.location.saLocationId === undefined) {
      alert('Please Select Location');
      this.router.navigate(['home/routinglocationcomponent']);
    } else if (this.serviceman.saBpartnerId === undefined) {
      alert('Please Select Servicesmen');
    } else if (this.table.name === undefined) {
      alert('Please Select Table');
    } else {
      this.dataserviceservice.changeInhouse(reservationlinee);
      this.router.navigate(['home/routingitemcomponent']);
    }
  }

}
