import { Component, OnInit, ElementRef } from '@angular/core';
import { Table } from 'src/app/dto/table';
import { TableService } from 'src/app/service/table.service';
import * as utils from 'src/app/componant/mainui/navbar/navbar.component';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Serviceman } from 'src/app/dto/serviceman';
import { Location } from 'src/app/dto/location';
import { Router } from '@angular/router';
import { ReservationLine } from 'src/app/dto/reservationline';
import { Notification } from 'src/app/dto/Notification';


@Component({
  selector: 'app-routingtable',
  templateUrl: './routingtable.component.html',
  styleUrls: ['./routingtable.component.scss']
})

export class RoutingtableComponent implements OnInit {

  notification: Notification = new Notification;

  alltables: Array<Table> = [];
  tablee: Table;
  serviceman: Serviceman;
  location: Location;

  /// tempreservation
  reservationline: ReservationLine;


  constructor(
    private tableService: TableService,
    private dataserviceservice: DataserviceService,
    private elem: ElementRef,
    private router: Router) { }

  ngOnInit() {
    this.dataserviceservice.currentservicesmen.subscribe(servicesmen => this.serviceman = servicesmen);
    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);
    this.getAllActiveTableByLocationStatus();


  }

  getAllActiveTableByLocationStatus(): void {

    if (this.location.saLocationId === undefined || this.location.saLocationId === null) {

      /////// alert
      this.notification.image = 'warning.png';
      this.notification.title = 'Oooops !';
      this.notification.discription = 'Please Select Location Before Select Table';
      this.dataserviceservice.changenotification(this.notification);
      document.getElementById('tempbutton').click();

      this.router.navigate(['home/routinglocationcomponent']);
    } else if (this.serviceman.saBpartnerId === undefined || this.serviceman.saBpartnerId === null) {

      /////// alert
      this.notification.image = 'warning.png';
      this.notification.title = 'Oooops !';
      this.notification.discription = 'Please Select ServiceMen Before Select Table';
      this.dataserviceservice.changenotification(this.notification);
      document.getElementById('tempbutton').click();

      this.router.navigate(['home/routingservicemancomponent']);
    } else {
      this.tableService.getAllActiveTableByLocationStatus(this.location.saLocationId).subscribe(
        (result) => {
          this.alltables = result;
          console.log(this.alltables);
        }
      );
    }
  }

  tableclickAction(table: Table): void {
    if (this.serviceman.saBpartnerId === undefined) {

      /////// alert
      this.notification.image = 'warning.png';
      this.notification.title = 'Oooops !';
      this.notification.discription = 'Please Select Servicesmen';
      this.dataserviceservice.changenotification(this.notification);
      document.getElementById('tempbutton').click();

    } else if (this.location.saLocationId === undefined) {

      /////// alert
      this.notification.image = 'warning.png';
      this.notification.title = 'Oooops !';
      this.notification.discription = 'Please Select Location';
      this.dataserviceservice.changenotification(this.notification);
      document.getElementById('tempbutton').click();

    } else {
      this.dataserviceservice.changeTable(table);

    }
  }

  outsideAction() {

    if (this.location.saLocationId === undefined) {

      /////// alert
      this.notification.image = 'warning.png';
      this.notification.title = 'Oooops !';
      this.notification.discription = 'Please Select Location';
      this.dataserviceservice.changenotification(this.notification);
      document.getElementById('tempbutton').click();

      this.router.navigate(['home/routinglocationcomponent']);
    } else if (this.serviceman.saBpartnerId === undefined) {

       /////// alert
       this.notification.image = 'warning.png';
       this.notification.title = 'Oooops !';
       this.notification.discription = 'Please Select ServiceMent';
       this.dataserviceservice.changenotification(this.notification);
       document.getElementById('tempbutton').click();

    } else {
      this.reservationline = new ReservationLine();
      this.reservationline.remark = 'Out Side';
      this.dataserviceservice.changeInhouse(this.reservationline);
      this.router.navigate(['home/routingitemcomponent']);
    }
  }
}


