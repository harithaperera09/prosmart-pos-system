import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportComponent } from './report/report.component';
import { ReportLayoutRouterModule } from './report-layout-router.module';
import { NavbarComponent } from '../navbar/navbar.component';

@NgModule({
  declarations: [
    ReportComponent,
    // NavbarComponent,
  ],
  imports: [
    CommonModule,
    ReportLayoutRouterModule,
  ]
})
export class ReportLayoutModule { }
