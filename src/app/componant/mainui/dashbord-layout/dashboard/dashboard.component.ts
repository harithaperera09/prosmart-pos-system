import { Component, OnInit, HostListener } from '@angular/core';
import { Alert } from 'selenium-webdriver';
import { OrderdetailsService } from 'src/app/service/orderdetails.service';
import { Table } from 'src/app/dto/table';
declare var $: any;
import { Router } from '@angular/router';
import { FavouriteCatorgory } from 'src/app/dto/favouritecatorgary';
import { FavouriteCatorgoryService } from 'src/app/service/favouritecatorgary.service';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Location } from 'src/app/dto/location';
import { Notification } from 'src/app/dto/Notification';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  allcatogaries: Array<FavouriteCatorgory> = [];
  favouritecatogaryis: FavouriteCatorgory;
  location: Location;
  searchtext = {};

  notification: Notification;

  /// keybord fuction
  // @HostListener('window:keydown', ['$event']) spaceEvent(event: any) {
  //   console.log(event.key);
  //   if (event.key === 'h') {
  //     console.log('40');
  //   } else if (event.key === 'a') {
  //     console.log('50');
  //   }
  // }

  computername: String;

  constructor(
    private favouritecatorgaryservice: FavouriteCatorgoryService,
    private router: Router,
    private orderdetailsservice: OrderdetailsService,
    private data: DataserviceService,
  ) { }




  ngOnInit() {
    this.notification = new Notification;
    this.data.currentLocation.subscribe(location => this.location = location);
    this.data.currentnotification.subscribe(notification => this.notification = notification);
  }


  viewReportUi() { }


  // printBillAction() {
  //   this.searchtext['printname'] = 'print1';

  //   this.orderdetailsservice.printallbill(this.searchtext).subscribe(
  //     (result) => {
  //       alert(result);
  //     }
  //   );
  // }

  toolAction() {
    if (this.location.saLocationId === null || this.location.saLocationId === undefined) {

      /////// alert
      this.notification.image = 'warning.png';
      this.notification.title = 'Oooops !';
      this.notification.discription = 'Please Select Location For Using Tool';
      this.data.changenotification(this.notification);
      document.getElementById('tempbutton').click();

    } else {
      this.router.navigate(['/tools']);
    }
  }

  getAllFavouriteCatorgary(): void {
    if (this.location.saLocationId === null || this.location.saLocationId === undefined) {

        /////// alert
        this.notification.image = 'warning.png';
        this.notification.title = 'Oooops !';
        this.notification.discription = 'Please Select Location For Using Favourite';
        this.data.changenotification(this.notification);
        document.getElementById('tempbutton').click();

    } else {
      this.router.navigate(['/home/faviuritemain']);
    }
  }
}
