import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToolComponent } from './tool/tool.component';
const routes: Routes = [
  {
    path: '',
    component: ToolComponent,
    children: [
      {
        path: 'settlementmain',
        loadChildren: 'src/app/componant/settlement/settlementmain-layout/settlementmain-layout.module#SettlementMainLayoutModule'
      },
      {
        path: 'tooldashbord',
        loadChildren: 'src/app/componant/routingUI/tooldashbord-layout/tooldashbord.module#ToolDashbordLayoutModule'
      },

      {
        path: 'reprint',
        loadChildren: 'src/app/componant/routingUI/routingtools/reprint-layout/reprint.module#ReprintModule'
      },

      {
        path: 'transfer',
        loadChildren: 'src/app/componant/routingUI/routingtools/billtransfer-layout/billtransfer.module#BilltransferModule'
      },


      {
        path: 'roomlist',
        loadChildren: 'src/app/componant/routingUI/routingtools/roominglist-layout/roominglist.module#RoominglistModule'
      },


      {
        path: 'movetable',
        loadChildren: 'src/app/componant/routingUI/routingtools/movetable-layout/movetable.module#MovetableModule'
      },
      {
        path: 'billsplit',
        loadChildren: 'src/app/componant/routingUI/routingtools/billsplit-layout/billsplit.module#BillSplitModule'
      },
      {
        path: 'createtable',
        loadChildren: 'src/app/componant/routingUI/routingtools/createtable-layout/createtable.module#CreatetableModule'
      },
      {
        path: 'creditcustomer',
        loadChildren: 'src/app/componant/routingUI/routingtools/creditcustomer-layout/creditcustomer.module#CreditcustomerModule'
      },
      {
        path: 'gustspecials',
        loadChildren: 'src/app/componant/routingUI/routingtools/guestspecials-layout/guestspecials.module#GuestspecialsModule'
      },
      {
        path: 'margebill',
        loadChildren: 'src/app/componant/routingUI/routingtools/mergebill-layout/mergebill.module#MergebillModule'
      },
      {
        path: 'setting',
        loadChildren: 'src/app/componant/routingUI/routingtools/mergebill-layout/mergebill.module#MergebillModule'
      },
      {
        path: 'cashin',
        loadChildren: 'src/app/componant/routingUI/routingtools/cashin-layout/cashin.module#CashinModule'
      },
      {
        path: 'cashout',
        loadChildren: 'src/app/componant/routingUI/routingtools/cashout-layout/cashout.module#CashoutModule'
      },
    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ToolLayoutRouterModule { }
