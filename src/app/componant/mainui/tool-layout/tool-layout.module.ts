import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettlementnavComponent } from '../navbar/othernav/settlementnav/settlementnav.component';
import { ToolComponent } from './tool/tool.component';
import { ToolLayoutRouterModule } from './tool-layout-router.module';

@NgModule({
  declarations: [
    ToolComponent
  ],
  imports: [
    CommonModule,
    ToolLayoutRouterModule,
    // FormsModule

  ]
})
export class ToolLayoutModule { }
