import { Component, OnInit } from '@angular/core';
import { Notification } from 'src/app/dto/Notification';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/service/dataservice.service';

@Component({
  selector: 'app-tool',
  templateUrl: './tool.component.html',
  styleUrls: ['./tool.component.scss']
})
export class ToolComponent implements OnInit {

  notification: Notification;
  constructor(private router: Router,
    private data: DataserviceService) { }

  ngOnInit() {
    this.router.navigate(['tools/tooldashbord']);
  }



}
