import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettlementnavComponent } from './settlementnav.component';

describe('SettlementnavComponent', () => {
  let component: SettlementnavComponent;
  let fixture: ComponentFixture<SettlementnavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettlementnavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementnavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
