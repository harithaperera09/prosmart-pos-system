import { Component, OnInit, ElementRef } from '@angular/core';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { Serviceman } from 'src/app/dto/serviceman';
import { Table } from 'src/app/dto/table';
import { Location } from 'src/app/dto/location';
import { PriceLevel } from 'src/app/dto/PriceLevel';
import { PriceLevelService } from 'src/app/service/pricelevel.service';
import { ReservationLine } from 'src/app/dto/reservationline';
declare var $: any;

@Component({
  selector: 'app-reportnav',
  templateUrl: './reportnav.component.html',
  styleUrls: ['./reportnav.component.scss']
})
export class ReportnavComponent implements OnInit {

  allpricelevel: Array<PriceLevel> = [];

  InsideOrOuteside: String;

  serviceman: Serviceman;
  table: Table;
  location: Location;
  pricelevel: PriceLevel;
  reservationline: ReservationLine;

  constructor(
    private elementref: ElementRef,
    private pricelevelservice: PriceLevelService,
    private dataserviceservice: DataserviceService) { }

  ngOnInit() {
    this.dataserviceservice.currentservicesmen.subscribe(servicesmen => this.serviceman = servicesmen);
    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);
    this.dataserviceservice.currenttable.subscribe(table => this.table = table);
    this.dataserviceservice.curentPriceLevel.subscribe(pricelevel => this.pricelevel = pricelevel);
    this.dataserviceservice.currentinhouse.subscribe(reservationline => {
      if (reservationline.remark === 'Out Side') {
        this.InsideOrOuteside = 'Out Side';
      } else {
        this.InsideOrOuteside = 'In Side';
      }
    });

    this.InsideOrOuteside = 'Out Side';

    this.dataserviceservice.curentsettclearProceed.subscribe(a => {
      this.table = new Table();
      this.serviceman = new Serviceman();
      this.pricelevel = new PriceLevel();
      this.getAllPriceLevelForPos();
    });



    this.getAllPriceLevelForPos();
  }

  getAllPriceLevelForPos(): void {
    this.pricelevelservice.getAllPriceLevelForPos().subscribe(
      (result) => {
        result.forEach(element => {
          if (element.name === 'Guest SP+(+S+V)') {
            this.pricelevel = element;
            this.dataserviceservice.changePriceLevel(element);
          }
        });
      }
    );
  }

}
