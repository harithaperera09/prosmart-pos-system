
import { Component, OnInit, ElementRef } from '@angular/core';
import { MajorcategoryService } from 'src/app/service/majorcategory.service';
import { Item } from 'src/app/dto/item';
import { Location } from 'src/app/dto/location';
import { ItemserviceService } from 'src/app/service/itemservice.service';
declare var $: any;
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/service/dataservice.service';

@Component({
  selector: 'app-catagory',
  templateUrl: './catagory.component.html',
  styleUrls: ['./catagory.component.scss']
})
export class CatagoryComponent implements OnInit {

  location: Location;

  majorKey: String;
  subKey: String;
  itmKey: String;

  items: Array<Item> = [];
  itemsmap = new Map();
  item: Item;

  majorcategorysmap = new Map();
  majorcategorys: Array<Item> = [];
  majorcategory: Item;

  subcategorysmap = new Map();
  subcategorys: Array<Item> = [];
  subcategory: Item;


  constructor(private itemservice: ItemserviceService,
    private majorcategoryservice: MajorcategoryService,
    private dataserviceservice: DataserviceService,
    private elem: ElementRef,
    private router: Router) {
  }

  ngOnInit() {
    this.dataserviceservice.currentLocation.subscribe(location => this.location = location);
    this.getAllActiveTableByLocationStatus();
  }


  getAllActiveTableByLocationStatus(): void {
      this.itemservice.getAllActiveItemMasterByLocation(this.location.saLocationId).subscribe(
        (result) => {
          this.items = result;
          this.makeSubcategoryAndMajorCategory();
          return;
        }
      );

  }

  makeSubcategoryAndMajorCategory(): void {
    console.log('make2');
    this.items.forEach(element => {
      this.majorKey = element.main_category_id;
      this.subKey = element.sub_category_id;
      this.itmKey = element.searchKey;

      this.itemsmap.set(this.itmKey, element);

      if (this.majorcategorysmap.size === 0) {
        this.majorcategorysmap.set(this.majorKey, element);
        this.majorcategorys.push(element);
      } else {
        if (!this.majorcategorysmap.has(this.majorKey)) {
          this.majorcategorysmap.set(this.majorKey, element);
          this.majorcategorys.push(element);
        }
      }

      if (this.subcategorysmap.size === 0) {
        this.subcategorysmap.set(this.subKey, element);
        this.subcategorys.push(element);
      } else {
        if (!this.subcategorysmap.has(this.subKey)) {
          this.subcategorysmap.set(this.subKey, element);
          this.subcategorys.push(element);
        }
      }
    });

    this.dataserviceservice.changeItemList(this.items);
  }



  majorcategoryClickAction(majorcategory: Item) {
    this.subcategorysmap = new Map();
    this.subcategorys = new Array();
    this.majorcategorysmap.forEach(element => {
      if (majorcategory.sub_category_id === element.sub_category_id) {
        this.subKey = element.sub_category_id;
        this.subcategorysmap.set(this.subKey, element);
        this.subcategorys.push(element);
      }
    });
  }

  subCategoryClickAction(subcategory: Item) {
    this.items = new Array();
    // this.itemsmap = new Map();

    this.itemsmap.forEach(element => {
      if (element.sub_category_id === subcategory.sub_category_id) {
        this.items.push(element);
        this.dataserviceservice.changeItemList(this.items);
      }
    });


  }

}
