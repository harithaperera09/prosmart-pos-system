import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { SettlementService } from 'src/app/service/settlement.service';
import { Posting } from 'src/app/dto/posting';
import { Settlement } from 'src/app/dto/settlement';
import { Notification } from 'src/app/dto/Notification';
declare var $: any;

@Component({
  selector: 'app-settlement',
  templateUrl: './settlement.component.html',
  styleUrls: ['./settlement.component.scss']
})
export class SettlementComponent implements OnInit {

  settlement: Settlement;
  posting: Posting;

  notification: Notification;

  constructor(
    private router: Router,
    private dataserviceservice: DataserviceService,
    private settlementservice: SettlementService,
  ) { }

  ngOnInit() {

    this.notification = new Notification;

    this.dataserviceservice.curentsettlementcash.subscribe(settlement => {
      this.settlement = settlement;

      if (settlement.saPosMasterId === null || settlement.saPosMasterId === undefined) {
        $('#Cancelation').prop('disabled', true);
        $('#pricelavel').prop('disabled', true);
        $('#viewkot').prop('disabled', true);
        $('#print').prop('disabled', true);
        $('#settle').prop('disabled', true);
        $('#discount').prop('disabled', true);
      } else {
        if (settlement.status === 2) {
          $('#Cancelation').prop('disabled', false);
          $('#pricelavel').prop('disabled', false);
          $('#viewkot').prop('disabled', false);
          $('#print').prop('disabled', false);
          $('#settle').prop('disabled', false);
          $('#discount').prop('disabled', false);
        } else if (settlement.status === 1) {
          $('#Cancelation').prop('disabled', false);
          $('#pricelavel').prop('disabled', false);
          $('#viewkot').prop('disabled', false);
          $('#print').prop('disabled', false);
          $('#settle').prop('disabled', true);
          $('#discount').prop('disabled', false);
        }
      }


      // if (settlement.saPosMasterId === null || settlement.saPosMasterId === undefined) {
      //   $('#Cancelation').prop('disabled', true);
      //   $('#pricelavel').prop('disabled', true);
      //   $('#viewkot').prop('disabled', true);
      //   $('#print').prop('disabled', true);
      //   $('#settle').prop('disabled', true);
      //   $('#discount').prop('disabled', true);
      // } else {
      //   $('#Cancelation').prop('disabled', false);
      //   $('#pricelavel').prop('disabled', false);
      //   $('#viewkot').prop('disabled', false);
      //   $('#settle').prop('disabled', true);
      //   $('#print').prop('disabled', false);
      //   $('#discount').prop('disabled', false);
      // }
      // if (settlement.status !== undefined || settlement.status !== null || settlement.status === 2) {
      //   alert('enable' + settlement.status);
      //   $('#settle').prop('disabled', false);
      // } else if (settlement.status !== undefined || settlement.status !== null || settlement.status === 1) {
      //   alert('disabled' + settlement.status);
      //   $('#settle').prop('disabled', true);
      // }
    });

    this.router.navigate(['/settlement/settlementmain']);



  }


  settlemetnAction() {
    this.posting = new Posting();

    this.settlementservice.chnageprintstatus(this.settlement).subscribe(
      (result) => {
        alert(result);
      }
    );
  }

  goHomeAction() {
    this.router.navigate(['/home']);
  }

  printAction() {

    this.settlementservice.chnageprintstatus(this.settlement).subscribe(
      (result) => {
        alert(result);
        this.notification.title = 'RefeshSettlementMain';
        this.dataserviceservice.changenotification(this.notification);
        $('#settle').prop('disabled', false);
      }
    );
  }



  viewkot() {

  }
}


