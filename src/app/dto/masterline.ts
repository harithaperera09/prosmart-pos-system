export class MasterLine {
  saPosMasterLineId: number;
  saClientId: number;
  saOrgId: number;
  isActive: boolean;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  searchKey: String;
  saPosMasterId: number;
  tax1: number;
  tax2: number;
  tax3: number;
  tax4: number;
  tax5: number;
  tax6: number;
  tax7: number;
  tax8: number;
  tax9: number;
  tax10: number;
  itemMasterId: number;
  quentity: number;
  orderTicketNo: number;
  saPosOrderTicketTypeId: number;
  unitPrice: number;
  isDelivered: boolean;
  isPriceChanged: boolean;
  isOrderTicketPrint: boolean;
  isDiscountPercent: boolean;
  discountAmount: number;
  discountPercent: number;
  discountRemark: String;
  isCancel: boolean;
  canceledDate: Date;
  cancelApprovedSaUserId: number;
  canceledSaUserId: number;
  canceledRemark: String;
  canceledMachine: String;
  createdMachine: String;
  remark: String;


  clientName: String;
  orgName: String;
  posMasterName: String;
  posOrderTicketTypeName: String;
  cancelApprovedUserName: String;
  canceledUserName: String;

  /// for seleted row
  isTableSelected: boolean;
}
