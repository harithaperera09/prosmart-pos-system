export class Master {
  saPosMasterId: number;
  saClientId: number;
  saOrgId: number;
  isActive: boolean;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  searchKey: String;
  oldsearchkey: String;
  saPosTableId: number;
  saLocationId: number;
  servicemanSaBpartnerId: number;
  guestSaBpartnerId: number;
  saFoReservationLineId: number;
  saFoRoomId: number;
  saPriceLevelId: number;
  adultCount: number;
  isCancel: boolean;
  canceledDate: Date;
  cancelApprovedSaUserId: number;
  canceledSaUserId: number;
  canceledRemark: String;
  canceledMachine: String;
  createdMachine: String;
  status: number;
  billStartTime: Date;
  billEndTime: Date;
  isDiscountPercent: boolean;
  discountMethod: String;
  discountAmount: number;
  discountPercent: number;
  discountRemark: String;
  subTotal: number;
  billPrintCount: number;
  printMachine: String;
  settledMachine: String;
  recallStatus: number;
  splitFromSaPosMasterId: number;
  remark: String;

  clientName: String;
  orgName: String;
  posTableName: String;
  locationName: String;
  servicemanBpartnerName: String;
  guestBpartnerName: String;
  roomName: String;
  priceLevelName: String;
  cancelApprovedUserName: String;
  canceledUserName: String;

  /// for seleted row
  isTableSelected: boolean;

}
