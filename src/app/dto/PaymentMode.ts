export class PaymentMode {
  saPaymentModeId: number;
  saClientId: number;
  saOrgId: number;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  isActive: boolean;
  searchKey: String;
  name: String;
  description: String;
  sortOrder: number;
  docCodeGeneratorId: number;
  isPosActive: boolean;
  remark: String;
  //
  clientName: String;
  orgName: String;
  docCodeSearchKey: String;

}
