
export class Settlement {
  saPosMasterId: number;
  ClientId: number;
  saOrgId: number;
  isActive: boolean;
  created: Date;
  createdBy: number;
  updated: Date;
  updatedBy: number;
  searchKey: String;
  oldsearchkey: String;
  PosTableId: number;
  LocationId: number;
  servicemanSaBpartnerId: number;
  guestSaBpartnerId: number;
  saFoReservationLineId: number;
  FoRoomId: number;
  PriceLevelId: number;
  adultCount: number;
  isCancel: boolean;
  canceledDate: Date;
  cancelApprovedSaUserId: number;
  canceledSaUserId: number;
  canceledRemark: String;
  canceledMachine: String;
  createdMachine: String;
  status: number;
  billStartTime: Date;
  billEndTime: Date;
  isDiscountPercent: boolean;
  discountMethod: String;
  discountAmount: number;
  discountPercent: number;
  discountRemark: String;
  subTotal: number;
  billPrintCount: number;
  printMachine: String;
  settledMachine: String;
  recallStatus: number;
  splitFromSaPosMasterId: number;
  remark: String;


  clientName: String;
  orgName: String;
  posTableName: String;
  locationName: String;
  servicemanBpartnerName: String;
  guestBpartnerName: String;
  roomName: String;
  priceLevelName: String;
  cancelApprovedUserName: String;
  canceledUserName: String;
  orderTicketSearchKey: String;
  subTotalWithoutTax: number;

  posMasterLineId: number;
  // sa_pos_master_line_id: number;
  tax1: number;
  tax2: number;
  tax3: number;
  tax4: number;
  tax5: number;
  tax6: number;
  tax7: number;
  tax8: number;
  tax9: number;
  tax10: number;
  itemMasterId: number;
  quentity: number;
  orderTicketNo: String;
  PosOrderTicketTypeId: number;
  unitPrice: number;
  isDelivered: boolean;
  isPriceChanged: boolean;
  isOrderTicketPrint: boolean;

  sellingPrice: number;
  itemMasterName: String;

  /// for seleted row
  isTableSelected: boolean;


}
